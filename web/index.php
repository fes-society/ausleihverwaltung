<?php

date_default_timezone_set('Europe/Berlin');
ini_set('display_errors', true);
ini_set('session.cookie_httponly', 1);

require_once '../flight/Flight.php';
require_once '../app/app.php';

Flight::set('flight.views.path', '../views');
Flight::start();
