function refreshDelayedCount () {
  window.fetch('/delayed')
    .then((response) => {
      return response.json()
    })
    .then((data) => {
      const element = document.getElementById('delayed-badge')

      element.classList.remove('is-delayed')
      element.innerText = ''

      if (data > 0) {
        if (data === 1) {
          element.innerText = 'Ein überzogener Verleihprozess'
        } else {
          element.innerText = data + ' überzogene Verleihprozesse'
        }
        element.classList.add('is-delayed')
      }
      const oneMinute = 60000
      setTimeout(refreshDelayedCount, oneMinute)
    })
}

document.addEventListener('DOMContentLoaded', function (event) {
  refreshDelayedCount()
})

function passwordOnChange (evt) {
  const classPrefix = 'password-strength-'
  const parent = this.parentNode
  const meter = parent.querySelector('.meter')
  const result = zxcvbn(this.value)

  const strength = {
    0: (this.value.length > 0) ? 'Schlecht ----' : '',
    1: 'Schlecht X---',
    2: 'Schwach XX--',
    3: 'Gut XXX-',
    4: 'Stark XXXX'
  }

  meter.innerText = strength[result.score]
  meter.title = result.feedback.suggestions + '\n' + result.feedback.warning

  for (const key of Object.keys(strength)) {
    parent.classList.remove(classPrefix + key)
  }

  parent.classList.add(classPrefix + result.score)
}
document.getElementById('password').addEventListener('input', passwordOnChange, false)
