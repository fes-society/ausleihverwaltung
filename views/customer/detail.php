<header>
    <div>
        <a class="button"
           href="<?=Flight::util()::action('@index')?>">
           Zurück zur Übersicht
        </a>
    </div>
</header>

<form method="post">
    <div class="with-sidebar">
        <div class="flexible-form">
            <div class="flexible-form-row">
                <fieldset>
                    <legend>Person</legend>
                    <label for="firstname">
                         <div class="title">Vorname</div>
                         <input name="firstname"
                                value="<?=htmlspecialchars($customer->firstname)?>" />
                    </label>
                    <label for="lastname">
                         <div class="title">Nachname</div>
                         <input name="lastname"
                                value="<?=htmlspecialchars($customer->lastname)?>" />
                    </label>
                    <label for="date_of_birth">
                         <div class="title">Geburtsdatum</div>
                         <input name="date_of_birth"
                                type="date"
                                value="<?=htmlspecialchars($customer->date_of_birth)?>" />
                    </label>
                    <label for="identity_card_number">
                         <div class="title">Personalausweiß</div>
                         <input name="identity_card_number"
                                value="<?=htmlspecialchars($customer->identity_card_number)?>" />
                    </label>
                </fieldset>

                <fieldset>
                    <legend>Kontakt</legend>
                    <label for="email">
                         <div class="title">E-Mail</div>
                         <input name="email"
                                value="<?=htmlspecialchars($customer->email)?>" />
                    </label>
                    <label for="phone_number">
                         <div class="title">Telefonnummer</div>
                         <input name="phone_number"
                                value="<?=htmlspecialchars($customer->phone_number)?>" />
                    </label>
                    <label for="phone_number_legal_guardian">
                         <div class="title">Telefonnummer Erziehungsberechtigt</div>
                         <input name="phone_number_legal_guardian"
                                value="<?=htmlspecialchars($customer->phone_number_legal_guardian)?>" />
                    </label>
                </fieldset>

                <fieldset <?=(Flight::session()->is_admin()) ? '' : 'disabled' ?>>
                    <legend>Admin Einstellungen</legend>
                    <label>
                         <div class="title">Kundennummer</div>
                         <input disabled value="<?=htmlspecialchars($customer->id)?>" />
                    </label>
                    <label for="accepted_external_use_conditions">
                         <div class="title">Bedingungen für externen Verleih zugestimmt</div>
                         <select name="accepted_external_use_conditions"
                                 id="accepted_external_use_conditions">
                         <?php
                             $use_condition_options = [
                                 0 => 'Nein',
                                 1 => 'Ja'
                             ];
                         ?>
                         <?php foreach ($use_condition_options as $option_key => $option_text): ?>
                             <option
                                 value="<?=htmlspecialchars($option_key)?>"
                                 <?=($customer->accepted_external_use_conditions == $option_key) ? 'selected' : '' ?>>
                                 <?=htmlspecialchars($option_text)?>
                             </option>
                         <?php endforeach; ?>
                         </select>
                    </label>
                    <label for="trust_level">
                         <div class="title">Vertrauenslevel</div>
                         <input name="trust_level"
                                id="trust_level"
                                value="<?=htmlspecialchars($customer->trust_level)?>" />
                    </label>
                </fieldset>

            </div>
            <div class="flexible-form-row">

                <fieldset>
                    <legend>Adresse</legend>
                    <label for="street">
                         <div class="title">Straße</div>
                         <input name="street"
                                value="<?=htmlspecialchars($customer->street)?>" />
                    </label>
                    <label for="street_number">
                         <div class="title">Hausnummer</div>
                         <input name="street_number"
                                value="<?=htmlspecialchars($customer->street_number)?>" />
                    </label>
                    <label for="postcode">
                         <div class="title">PLZ</div>
                         <input name="postcode"
                                value="<?=htmlspecialchars($customer->postcode)?>" />
                    </label>
                    <label for="place">
                         <div class="title">Ort</div>
                         <input name="place"
                                value="<?=htmlspecialchars($customer->place)?>" />
                    </label>
                </fieldset>

                <fieldset>
                    <legend>Sicherheit</legend>
                    <label for="role">
                        <div class="title">Rolle</div>
                        <select name="role"
                                <?=(!Flight::session()->is_admin() || Flight::session()->user()->id == $customer->id) ? 'disabled' : '' ?>
                                id="role">
                        <?php foreach ($customer->get_allowed_roles() as $option_key => $option_text): ?>
                            <option
                                value="<?=htmlspecialchars($option_key)?>"
                                <?=($customer->role == $option_key) ? 'selected' : '' ?>>
                                <?=htmlspecialchars($option_text)?>
                            </option>
                        <?php endforeach; ?>
                        </select>
                    </label>
                    <label for="username">
                        <div class="title">Benutzername</div>
                        <input
                            name="username"
                            id="username"
                            value="<?=htmlspecialchars($customer->username)?>" />
                    </label>
                    <label for="password">
                        <div class="title">
                            Passwort
                            <small><?=(empty($customer->password)) ? 'Nicht gesetzt' : 'Gesetzt' ?> <span class="meter"></span></small>
                        </div>
                        <input
                            name="password"
                            id="password"
                            type="password"
                            value="" />
                    </label>
                </fieldset>
            </div>

            <footer class="flexible-form-row">
                <div>
                    <?php if(!empty($customer->id) && Flight::session()->user()->id != $customer->id && Flight::session()->is_admin()): ?>
                        <a class="button"
                           href="<?=Flight::util()::action('@delete', ['id' => htmlspecialchars($customer->id)])?>" onclick="return confirm('Wollen Sie diesen Kunden wirklich löschen?');">Löschen</a>
                    <?php endif; ?>
                </div>
                <div>
                    <button class="button"
                            type="reset">Zurücksetzen</button>
                    <button class="button primary"
                            type="submit">Speichern</button>
                </div>
            </footer>
        </div>
        <aside>
            <fieldset>
                <legend>Statistik</legend>
                <p>Aktive Verleihe: <?=$customer->active_issues()?></p>
                <p>Überzogene aktive Verleihe: <?=$customer->delayed_process()?></p>
                <p>Zurückgegebene Verleihe: <?=$customer->returned_process()?></p>
                <p>Verspätet zurückgegebene Verleihe: <?=$customer->returned_delayed_process()?></p>
            </fieldset>
            <?=DetailMetaController::show($customer)?>
        </aside>
    </div>

</form>
