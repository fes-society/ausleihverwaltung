<header>
    <div></div>
    <div>
        <?php if (!empty($query)): ?>
        <span><?=Flight::util()::quantize('%s Suchergebnis', '%s Suchergebnisse', $total_result_count)?></span>
        <a class="button"
           href="<?=Flight::util()::action('@index')?>">Suche zurücksetzen</a>
       <?php endif; ?>
        <a class="button"
           href="<?=Flight::util()::action('@create')?>">Neuen Kunden erstellen</a>
    </div>
</header>

<form class="search-form">
   <label for="query"
          class="query">
       <div class="title">Suchen</div>
       <input name="query"
              id="query"
              value="<?=htmlspecialchars($query)?>"/>
   </label>
   <button class="button" type="submit">Suchen</button>
</form>

<?php if (empty($customers)): ?>
   Ihre Suche hatte keine Ergebnisse
<?php else: ?>

    <?=PaginationController::show(Flight::util()::action('@index'), $total_result_count, count($customers))?>

    <table class="table">
        <thead>
            <tr>
                <th>Kundennr.</th>
                <th>Name</th>
                <th>E-Mail</th>
                <th>Benutzername</th>
                <th>Rolle</th>
                <th title="Vertrauenslevel">Vertrauen</th>
                <th title="Bedingungen für externen Verleih zugestimmt">Extern</th>
                <th title="Aktive Verleihungen">Aktiv</th>
                <th></th>
            </tr>
        </thead>
        <tbody>
        <?php foreach ($customers as $customer): ?>
            <tr>
                <td><?=htmlspecialchars($customer->id)?></td>
                <td><?=htmlspecialchars($customer->name())?></td>
                <td><?=htmlspecialchars($customer->email)?></td>
                <td><?=htmlspecialchars($customer->username)?></td>
                <td><?=$customer->get_allowed_roles()[$customer->role]?></td>
                <td><?=htmlspecialchars($customer->trust_level)?></td>
                <td><?=htmlspecialchars($customer->accepted_external_use_conditions)?></td>
                <td><?=htmlspecialchars($customer->active_issues())?></td>

                <td><a href="<?=Flight::util()::action('@edit', ['id' => htmlspecialchars($customer->id)])?>">Bearbeiten</a></td>
            </tr>
        <?php endforeach; ?>
        </tbody>
    </table>

    <?=PaginationController::show(Flight::util()::action('@index'), $total_result_count, count($customers))?>

<?php endif; ?>
