<p><?=$statistics?></p>

<header>
    <div><!-- LEFT BUTTONS --></div>
    <div>
        <?php if (!empty($query_inventorynumber) || !empty($query_device_type_id) || !empty($query_customer) || !empty($query_status)): ?>
        <span><?=Flight::util()::quantize('%s Suchergebnis', '%s Suchergebnisse', $amount_result)?></span>
        <a class="button"
           href="<?=Flight::util()::action('@active')?>">Suche zurücksetzen</a>
        <?php endif; ?>
    </div>
</header>

<form class="search-form">
    <label for="query_status"
           class="query">
        <div class="title">Status</div>
        <select name="query_status">
            <option value="">-</option>
            <?php
                $all_status = [
                    'planned' => 'Geplant',
                    'reserved' => 'Reserviert',
                    'collected' => 'Abgeholt',
                    'delayed' => 'Überzogen'
                ];
            ?>
            <?php foreach($all_status as $key => $status): ?>
                <option value="<?=$key?>"
                    <?=($key == $query_status) ? 'selected="selected"' : ''?>>
                    <?=$status?>
                </option>
            <?php endforeach; ?>
        </select>
    </label>

    <label for="query_device_type"
           class="query">
        <div class="title">Gerätetyp</div>
        <select name="query_device_type_id">
            <option value="">-</option>
            <?php foreach($all_types as $type): ?>
                <option value="<?=htmlspecialchars($type->id) ?>"
                    <?=($type->id == $query_device_type_id) ? 'selected="selected"' : ''?>>
                    <?=htmlspecialchars($type->title)?>
                </option>
            <?php endforeach; ?>
        </select>
    </label>

    <label for="query_inventorynumber"
           class="query">
        <div class="title">Gerät Inventarnummer</div>
        <input name="query_inventorynumber"
               id="query_inventorynumber"
               value="<?=htmlspecialchars($query_inventorynumber)?>"/>
    </label>

    <?php if (!Flight::session()->is_customer()): ?>
        <label for="query_customer"
               class="query">
            <div class="title">Kundennummer oder Kundenname</div>
            <input name="query_customer"
                   id="query_customer"
                   value="<?=htmlspecialchars($query_customer)?>"/>
        </label>
    <?php endif; ?>

    <button class="button"
            type="submit">Suchen</button>
</form>

<?php if (empty($rental_process)): ?>
    Es gibt keine aktiven Verleihe
<?php else: ?>

    <?=PaginationController::show(Flight::util()::action('@active'), $amount_result, count($rental_process))?>

    <table class="table">
        <thead>
            <tr>
                <th>Inventarnr.</th>
                <th>Gerätetyp</th>
                <th>Hersteller</th>
                <th>Kunde</th>
                <th>Geplante Zeit</th>
                <th>Abgeholt</th>
                <th>Kommentar</th>
                <th>Status</th>
                <th></th>
            </tr>
        </thead>

        <tbody>
        <?php foreach ($rental_process as $process): ?>
            <tr class="<?=($process->is_delayed() && !$process->is_returned()) ? 'delayed' : '' ?>">

                <td>
                    <a href="<?=Flight::util()::action('DeviceController@edit', ['id' => htmlspecialchars($process->device()->id)])?>">
                        <?=htmlspecialchars($process->device()->inventory_number)?>
                    </a>
                </td>

                <td><?=htmlspecialchars($process->device()->type()->title)?></td>

                <td><?=htmlspecialchars($process->device()->manufacturer)?></td>

                <td>
                    <a href="<?=Flight::util()::action('CustomersController@edit', ['id' => htmlspecialchars($process->customer()->id)])?>">
                        <?=htmlspecialchars($process->customer()->name())?>
                        <?=($process->customer_id == $process->created_by) ? '*' : '' ?>
                    </a>
                </td>

                <?php
                    $name_created_by = Flight::util()::model('Customer')
                        ->find($process->created_by)
                        ->name();
                    $title_started_date = 'Startet '.Flight::util()::dume($process->start_time - time(), true)
                        .'&#10;Dauer: '.Flight::util()::dume($process->total_time())
                        .'&#10;Ersteller: '.htmlspecialchars($name_created_by)
                ?>
                <td title="<?=$title_started_date?>">
                    <?=date('d.m.Y H:i', $process->start_time)?>
                    -
                    <?=date('d.m.Y H:i', $process->end_time)?>
                </td>

                <td>
                    <?php if ($process->is_collected()): ?>
                        <?php
                            $name_collected_by = Flight::util()::model('Customer')
                                ->find($process->collected_by)
                                ->name();
                        ?>
                        <span title="Bearbeiter: <?=htmlspecialchars($name_collected_by)?>">
                            <?=$process->get_collected_time()?>
                        </span>
                    <?php else: ?>
                        -
                    <?php endif; ?>
                </td>

                <td>
                    <?php if (!empty($process->comment)): ?>
                        <div class="scrollable"><?=htmlspecialchars($process->comment)?></div>
                    <?php else: ?>
                        -
                    <?php endif; ?>
                </td>

                <td>
                    <?php if ($process->is_returned()): ?>
                        Zurückgegeben
                    <?php elseif ($process->is_collected() && $process->is_delayed()): ?>
                        <?=Flight::util()::dume($process->end_time - time())?> überzogen
                    <?php elseif ($process->is_reserved()): ?>
                        Reserviert seit <?=Flight::util()::dume($process->created_at - time())?>
                    <?php elseif ($process->is_collected()): ?>
                        Noch <?=Flight::util()::dume($process->end_time - time())?> ausgeliehen
                    <?php elseif ($process->is_planned()): ?>
                        Startet <?=Flight::util()::dume($process->start_time - time(), true)?>
                    <?php endif; ?>
                </td>

                <td>
                    <?php if ($process->is_collected() && !Flight::session()->is_customer()): ?>
                        <a href="<?=Flight::util()::action('@return', ['id' => htmlspecialchars($process->id)])?>">Zurückgeben</a>
                    <?php elseif ($process->is_reserved() && !Flight::session()->is_customer()): ?>
                        <a href="<?=Flight::util()::action('@collect', ['id' => htmlspecialchars($process->id)])?>">Abholen</a>
                    <?php endif; ?>
                    <?php if ($process->is_reserved() || $process->is_planned()): ?>
                        <a href="<?=Flight::util()::action('@delete', ['id' => htmlspecialchars($process->id)])?>" onclick="return confirm('Wollen Sie diesen Verleihprozess wirklich löschen?');">Löschen</a>
                    <?php endif; ?>
                </td>
            </tr>
        <?php endforeach; ?>
        </tbody>

    </table>

    <?=PaginationController::show(Flight::util()::action('@active'), $amount_result, count($rental_process))?>

    <small>* Der Kunde hat diesen Verleih selbst erstellt.</small>
<?php endif; ?>
