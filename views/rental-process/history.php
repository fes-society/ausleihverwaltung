<p><?=$statistics?></p>

<header>
    <div><!-- LEFT BUTTONS --></div>
    <div>
        <?php if (!empty($query_inventorynumber) || !empty($query_device_type_id) || !empty($query_customer)): ?>
        <span><?=Flight::util()::quantize('%s Suchergebnis', '%s Suchergebnisse', $amount_result)?></span>
        <a class="button"
           href="<?=Flight::util()::action('@history')?>">Suche zurücksetzen</a>
        <?php endif; ?>
        <form class="form-reset"
              method="POST"
              action="<?=Flight::util()::action('RentalProcessController@reset')?>">
            <button class="button"
                    onclick="return confirm('Wollen Sie wirklich alle Verleihe älter als ein Jahr unwiederruflich aus der Datenbank entfernen?');">
                Alle Verleihe älter als ein Jahr löschen
            </button>
        </form>
    </div>
</header>

<form class="search-form">
   <label for="query_inventorynumber"
          class="query">
       <div class="title">Gerät Inventarnummer</div>
       <input name="query_inventorynumber"
              id="query_inventorynumber"
              value="<?=htmlspecialchars($query_inventorynumber)?>"/>
   </label>
   <label for="query_device_type"
          class="query">
       <div class="title">Gerätetyp</div>
       <select name="query_device_type_id">
           <option value="">-</option>
           <?php foreach($all_types as $type): ?>
               <option value="<?=htmlspecialchars($type->id) ?>"
                       <?=($type->id == $query_device_type_id) ? 'selected="selected"' : ''?>>
                   <?=htmlspecialchars($type->title)?>
               </option>
           <?php endforeach; ?>
       </select>
   </label>
   <label for="query_customer"
          class="query">
       <div class="title">Kundennummer oder Kundenname</div>
       <input name="query_customer"
              id="query_customer"
              value="<?=htmlspecialchars($query_customer)?>"/>
   </label>
   <button class="button" type="submit">Suchen</button>
</form>

<?php if (empty($rental_process)): ?>
    Keine abgeschlossen Verleihprozesse gefunden
<?php else: ?>

    <?=PaginationController::show(Flight::util()::action('@history'), $amount_result, count($rental_process))?>

    <table class="table">
        <thead>
            <tr>
                <th>Inventarnr.</th>
                <th>Gerätetyp</th>
                <th>Hersteller</th>
                <th>Kunde</th>
                <th>Geplante Zeit</th>
                <th>Abgeholt</th>
                <th>Zurückgegeben</th>
                <th>Tatsächliche Dauer</th>
                <th>Verspätet</th>
                <th>Kommentar</th>
            </tr>
        </thead>
        <tbody>
        <?php foreach ($rental_process as $process): ?>
            <tr>
                <td>
                    <a href="<?=Flight::util()::action('DeviceController@edit', ['id' => htmlspecialchars($process->device()->id)])?>">
                        <?=htmlspecialchars($process->device()->inventory_number)?>
                    </a>
                </td>
                <td><?=htmlspecialchars($process->device()->type()->title)?></td>

                <td><?=htmlspecialchars($process->device()->manufacturer)?></td>

                <td>
                    <a href="<?=Flight::util()::action('CustomersController@edit', ['id' => htmlspecialchars($process->customer()->id)])?>">
                        <?=htmlspecialchars($process->customer()->name())?>
                    </a>
                </td>

                <?php
                    $name_created_by = Flight::util()::model('Customer')
                        ->find($process->created_by)
                        ->name();
                    $title_started_date = 'Startet '.Flight::util()::dume($process->start_time - time(), true)
                        .'&#10;Dauer: '.Flight::util()::dume($process->total_time())
                        .'&#10;Ersteller: '.htmlspecialchars($name_created_by)
                ?>

                <td title="<?=$title_started_date?>">
                    <?=date('d.m.Y H:i', $process->start_time)?>
                    -
                    <?=date('d.m.Y H:i', $process->end_time)?>
                </td>

                <td title="Bearbeiter: <?=htmlspecialchars(Flight::util()::model('Customer')->find($process->collected_by)->name())?>">
                    <?=date('d.m.Y H:i', $process->collected_at)?>
                </td>
                <td title="Bearbeiter: <?=htmlspecialchars(Flight::util()::model('Customer')->find($process->returned_by)->name())?>">
                    <?=date('d.m.Y H:i', $process->returned_at)?>
                </td>
                <td><?=Flight::util()::dume($process->actual_total_time())?></td>
                <td>
                    <?php if ($process->was_delayed()): ?>
                        <?=Flight::util()::dume($process->end_time - $process->returned_at)?>
                    <?php else: ?>
                        –
                    <?php endif; ?>
                </td>

                <td title="<?=htmlspecialchars($process->comment)?>">
                    <?php if (!empty($process->comment)): ?>
                        <u>Ja</u>
                    <?php else: ?>
                        -
                    <?php endif; ?>
                </td>

            </tr>
        <?php endforeach; ?>
        </tbody>
    </table>

    <?=PaginationController::show(Flight::util()::action('@history'), $amount_result, count($rental_process))?>

<?php endif; ?>
