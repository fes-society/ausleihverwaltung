<footer class="pagination">
    <?php if (!empty($link_previous)): ?>
        <a class="button" href="<?=$link_previous?>">☜ Vorherige Seite</a>
    <?php else: ?>
        <span></span>
    <?php endif; ?>

    <div class="info"
         title="Gezeigte Datensätze auf dieser Seite: <?=$amount_page?>">
        Seite <?=$current_page_number?> von <?=$max_page_number?>
    </div>

    <?php if (!empty($link_next)): ?>
        <a class="button" href="<?=$link_next?>">Nächste Seite ☞</a>
    <?php else: ?>
        <span></span>
    <?php endif; ?>
</footer>

