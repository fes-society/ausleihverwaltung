<header>
    <div></div>
    <div>
        <?php if (!empty($query)): ?>
        <a class="button"
           href="<?=Flight::util()::action('@index')?>">Suche zurücksetzen</a>
       <?php endif; ?>
        <a class="button"
           href="<?=Flight::util()::action('@create')?>">Neuen Gerätetyp erstellen</a>
    </div>
</header>

<form class="search-form">
   <label for="query"
          class="query">
       <div class="title">Suchen</div>
       <input name="query"
              id="query"
              value="<?=htmlspecialchars($query)?>"/>
   </label>
   <button class="button" type="submit">Suchen</button>
</form>

<?php if (empty($types)): ?>
   Ihre Suche hatte keine Ergebnisse
<?php else: ?>
    <table class="table">
        <thead>
            <tr>
                <th>Titel</th>
                <th>Beschreibung</th>
                <th></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($types as $type): ?>
                <tr>
                    <td><?=htmlspecialchars($type->title)?></td>
                    <td><?=htmlspecialchars($type->description)?></td>
                    <td><a href="<?=Flight::util()::action('@delete', ['id' => htmlspecialchars($type->id)])?>" onclick="return confirm('Wollen Sie diesen Gerätetyp wirklich löschen?');">Löschen</a>
                    <a href="<?=Flight::util()::action('@edit', ['id' => htmlspecialchars($type->id)])?>">Bearbeiten</a></td>
                </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
<?php endif; ?>
