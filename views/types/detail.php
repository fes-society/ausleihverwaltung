<header>
    <div>
        <a class="button"
           href="<?=Flight::util()::action('@index')?>">
           Zurück zur Übersicht
        </a>
    </div>
</header>

<form method="post">
    <label for="title">
        <div class="title">Titel</div>
        <input name="title"
               id="title"
               value="<?=htmlspecialchars($type->title)?>" />
    </label>
    <label for="description">
        <div class="title">Beschreibung</div>
        <input name="description"
               id="description"
               value="<?=htmlspecialchars($type->description)?>" />
    </label>

    <footer>
        <div>
            <?php if (!empty($type->id)): ?>
                <a class="button" href="<?=Flight::util()::action('@delete', ['id' => htmlspecialchars($type->id)])?>" onclick="return confirm('Wollen Sie diesen Gerätetyp wirklich löschen?');">Löschen</a>
            <?php endif; ?>
        </div>
        <div>
            <button class="button"
                    type="reset">Zurücksetzen</button>
            <button class="button primary"
            type="submit">Speichern</button>
        </div>
    </footer>
</form>
