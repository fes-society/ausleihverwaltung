<form class="search-form"
      method="post">
   <label for="inventory_number"
          class="query">
       <div class="title">Gerät Inventarnummer</div>
       <input name="inventory_number"
              id="inventory_number"
              autofocus
              required />
   </label>
   <button class="button primary"
           type="submit">Hinzufügen</button>
</form>

<form class="search-form">
<label for="query_device_type"
       class="query">
    <div class="title">Verfügbare Geräte nach Typ</div>
    <select name="query_device_type_id">
        <option value="">-</option>
        <?php foreach($all_types as $type): ?>
            <option value="<?=htmlspecialchars($type->id) ?>"
                <?=($type->id == $query_device_type_id) ? 'selected="selected"' : ''?>>
                <?=htmlspecialchars($type->title)?>
            </option>
        <?php endforeach; ?>
    </select>
</label>
<button class="button"
        type="submit">Suchen</button>
</form>

<?php if (empty($devices)): ?>
    Keine Geräte verfügbar. Wählen Sie einen anderen Gerätetypen aus.
<?php else: ?>
<table class="table">
    <thead>
    <tr>
        <th>Inventarnr.</th>
        <th>Gerätetyp</th>
        <th>Hersteller</th>
        <th>Zustand</th>
        <th>Zubehör</th>
        <th></th>
    </tr>
    </thead>

    <tbody>
    <?php foreach ($devices as $device): ?>
        <tr>
            <td><?=htmlspecialchars($device->inventory_number)?></td>
            <td><?=htmlspecialchars($device->type()->title)?></td>
            <td><?=htmlspecialchars($device->manufacturer)?></td>
            <td><?=htmlspecialchars($device->condition)?></td>
            <td><?=htmlspecialchars($device->equipment)?></td>
            <td>

                <form method="post">
                    <input name="inventory_number"
                           type="hidden"
                           value="<?=htmlspecialchars($device->inventory_number)?>" />
                    <button class="button small">Hinzufügen</button>
                </form>

            </td>
        </tr>
    <?php endforeach; ?>
    </tbody>

</table>
<?php endif; ?>

