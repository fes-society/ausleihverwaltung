<div class="select-time">
    <form method="post"
          class="flexible-form">

        <div class="flexible-form-row">
            <fieldset class="row">
                <legend>Von</legend>
                <label>
                    <div class="title">Datum</div>
                    <input value="<?=$start_date?>"
                           min="<?=date('Y-m-d')?>"
                           type="date"
                           name="start_date"
                           autofocus
                           required />
                </label>
                <label>
                    <div class="title">Zeit</div>
                    <input value="<?=$start_time?>"
                           type="time"
                           name="start_time"
                           required />
                </label>
            </fieldset>
            <fieldset class="row">
                <legend>Bis</legend>
                <label>
                    <div class="title">Datum</div>
                    <input value="<?=$end_date?>"
                           min="<?=date('Y-m-d')?>"
                           type="date"
                           name="end_date"
                           required />
                </label>
                <label>
                    <div class="title">Zeit</div>
                    <input value="<?=$end_time?>"
                           type="time"
                           name="end_time"
                           required />
                </label>
            </fieldset>

            <button class="button primary"
                    type="submit">Auswählen</button>
        </div>

    </form>

</div>
