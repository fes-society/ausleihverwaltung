<form method="post"
      action="<?=Flight::util()::action('@store_comment')?>">
   <label for="comment"
          class="query">
       <div class="title">Kommentar</div>
       <textarea name="comment"
                id="comment"
                autofocus><?=empty($data['comment']) ? '' : $data['comment'] ?></textarea>
   </label>
   <button class="button primary"
           type="submit">Speichern</button>
</form>
