<div class="checkout with-sidebar">

    <div class="selection-form">
        <?=$new_rental_process_content?>
    </div>

    <aside>

        <fieldset>
            <legend>
                Checkout
            </legend>

        <section class="selected-customer">
            <?php
                $customer_link_title = (empty($customer->id)) ? 'Kunde auswählen...' : 'Kunde';
                $customer_link_classes[] = 'title';
                $customer_link_classes[] = (!empty($devices) || Flight::session()->is_customer()) ? 'disabled' : '';
                $customer_link_classes[] = ($active_link == 'customer') ? 'active' : '';
            ?>
            <a href="<?=Flight::util()::action('@select_customer')?>"
               class="<?=implode($customer_link_classes, ' ')?>">
                <?=$customer_link_title?>
            </a>
                <?php if (!empty($customer->id)): ?>
                    <ul>
                        <li><?=htmlspecialchars($customer->name())?>, Kundennr. <?=htmlspecialchars($customer->id)?></li>
                        <li>Vertrauenslevel <?=htmlspecialchars($customer->trust_level)?></li>
                        <li>Hat Bedingungen für externen Verleih <?=($customer->accepted_external_use_conditions == 'yes') ? '<b>nicht</b>' : '' ?> zugestimmt
                        <li>
                            <?=sprintf('Hat %s%s',
                                Flight::util()::quantize('einen aktiven Verleih', '%s aktive Verleihe', $customer->active_issues()),
                                ($customer->delayed_process() > 0) ? Flight::util()::quantize(', davon ist einer überzogen', ', davon sind %s überzogen', $customer->delayed_process()) : ''
                            )?>
                        </li>
                    </ul>
                <?php endif; ?>
        </section>

        <section class="selected-time">
            <?php
                $time_link_title = (empty($time)) ? 'Zeitraum wählen...' : 'Zeitraum';
                $time_link_classes[] = 'title';
                $time_link_classes[] = (empty($customer->id) || !empty($devices)) ? 'disabled' : '';
                $time_link_classes[] = ($active_link == 'time') ? 'active' : '';
            ?>
            <a href="<?=Flight::util()::action('@select_time')?>"
               class="<?=implode($time_link_classes, ' ')?>">
                <?=$time_link_title?>
            </a>
            <p>
                <?php if (!empty($time)): ?>
                    <?=htmlspecialchars($time)?>
                <?php endif; ?>
            </p>
        </section>

        <section class="selected-devices">
            <?php
                $device_link_title = (empty($devices)) ? 'Geräte auswählen...' : Flight::util()::quantize('Ein Gerät', '%s Geräte', count($devices));
                $device_link_classes = ['title'];
                $device_link_classes[] = (empty($customer->id) || empty($time)) ? 'disabled' : '';
                $device_link_classes[] = ($active_link == 'device') ? 'active' : '';
            ?>
            <a href="<?=Flight::util()::action('@select_device')?>"
               class="<?=implode($device_link_classes, ' ')?>">
                <?=$device_link_title?>
            </a>
            <?php if (!empty($devices)): ?>
                <ul>
                    <?php foreach ($devices as $device): ?>
                        <li>
                            <?=htmlspecialchars($device->type()->title)?> <?=htmlspecialchars($device->inventory_number)?>
                            <a class="remove button"
                               href="<?=Flight::util()::action('@remove_device', ['id' => $device->id])?>">X</a>
                        </li>
                    <?php endforeach; ?>
                </ul>
            <?php endif; ?>
        </section>

        <section class="added-comment">
            <?php
                $comment_link_title = (empty($comment)) ? 'Kommentar hinzufügen...' : 'Kommentar bearbeiten...';
                $comment_link_classes = ['title'];
                $comment_link_classes[] = empty($devices) ? 'disabled' : '';
                $comment_link_classes[] = ($active_link == 'comment') ? 'active' : '';
            ?>
            <a href="<?=Flight::util()::action('@write_comment')?>"
               class="<?=implode($comment_link_classes, ' ')?>">
                <?=$comment_link_title?>
            </a>
        </section>

        <footer>
            <a class="button"
               href="<?=Flight::util()::action('@abort')?>">Abbrechen</a>
            <a class="button primary <?=(empty($customer->id) || empty($time) || empty($devices)) ? 'disabled' : '' ?>"
               href="<?=Flight::util()::action('@store')?>">Erstellen</a>
        </footer>

        </fieldset>

    </aside>
</div>
