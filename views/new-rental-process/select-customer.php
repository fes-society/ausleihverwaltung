<form class="search-form"
      method="post"
      action="<?=Flight::util()::action('@store_customer')?>">
   <label for="customer_number"
          class="query">
       <div class="title">Kundennummer</div>
       <input name="customer_number"
              id="customer_number"
              autofocus
              required />
   </label>
   <button class="button primary"
           type="submit">Auswählen</button>
</form>
