<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <link href="/main.css" rel="stylesheet" />
        <title><?=htmlspecialchars($title)?> - <?=Flight::get('app.title')?></title>
    </head>
    <body class="<?=Flight::util()::route_body_class()?>">
        <?=Flight::render('header')?>
        <h1><?=htmlspecialchars($title)?></h1>
        <?=Flight::render('alert', [
            'tempory_storage' => Flight::temp()
        ])?>
        <main>
            <?=$content?>
        </main>

    </body>
    <script type="text/javascript" src="/zxcvbn.js"></script>
    <script type="text/javascript" src="/main.js"></script>
</html>
