<header>
    <div>
        <a class="button"
           href="<?=Flight::util()::action('@index')?>">Zurück zur Übersicht</a>
    </div>
</header>

<form method="post" class="flexible-form">
    <div class="with-sidebar">
        <div class="flexible-form-row">
            <fieldset>
                <label for="inventory_number">
                    <div class="title">Inventar Nummer</div>
                    <input name="inventory_number"
                           id="inventory_number"
                           value="<?=htmlspecialchars($device->inventory_number)?>" />
                </label>
                <label for="device_type_id">
                    <div class="title">Gerätetyp</div>
                    <select name="device_type_id">
                        <option value="">-</option>
                        <?php foreach($all_types as $type): ?>
                        <option value="<?=htmlspecialchars($type->id) ?>"
                                <?=($type->id == $device->device_type_id) ? 'selected="selected"' : ''?>>
                            <?=htmlspecialchars($type->title)?>
                        </option>
                        <?php endforeach; ?>
                    </select>
                </label>
                <label for="condition">
                    <div class="title">Zustand</div>
                    <input name="condition"
                           id="condition"
                           value="<?=htmlspecialchars($device->condition)?>" />
                </label>
                <label for="manufacturer">
                    <div class="title">Hersteller</div>
                    <input name="manufacturer"
                           id="manufacturer"
                           value="<?=htmlspecialchars($device->manufacturer)?>" />
                </label>
                <label for="model">
                    <div class="title">Modell</div>
                    <input name="model"
                           id="model"
                           value="<?=htmlspecialchars($device->model)?>" />
                </label>
                <label for="trust_level">
                     <div class="title">Vertrauenslevel</div>
                     <input name="trust_level"
                            id="trust_level"
                            value="<?=htmlspecialchars($device->trust_level)?>" />
                </label>
            </fieldset>

            <fieldset>
                <label for="buy_date">
                    <div class="title">Kaufdatum</div>
                    <input name="buy_date"
                           id="buy_date"
                           type="date"
                           value="<?=htmlspecialchars($device->buy_date)?>" />
                </label>
                <label for="guarantee_duration">
                    <div class="title">Garantiedauer</div>
                    <input name="guarantee_duration"
                           id="guarantee_duration"
                           value="<?=htmlspecialchars($device->guarantee_duration)?>" />
                </label>
                <label for="storage_location">
                    <div class="title">Lagerort</div>
                    <input name="storage_location"
                           id="storage_location"
                           value="<?=htmlspecialchars($device->storage_location)?>" />
                </label>
                <label for="comment">
                    <div class="title">Kommentar</div>
                    <input name="comment"
                           id="comment"
                           value="<?=htmlspecialchars($device->comment)?>" />
                </label>
                <label for="equipment">
                    <div class="title">Zubehör</div>
                    <input name="equipment"
                           id="equipment"
                           value="<?=htmlspecialchars($device->equipment)?>" />
                </label>
            </fieldset>

        </div>
        <aside>

            <fieldset>
                <legend>Statistik</legend>
                <p>Aktuell verliehen: <?=($device->borrowed()) ? 'Ja' : 'Nein'?></p>
                <p>Geplant/Reserviert: <?=($device->reserved_or_planned()) ? 'Ja' : 'Nein'?></p>
                <p>Insgesamt verliehen: <?=$device->borrowed_total_amount()?></p>
                <p>Verspätet zurückgegeben: <?=$device->delayed_total_amount()?></p>
            </fieldset>

            <?=DetailMetaController::show($device)?>

        </aside>
    </div>

    <footer>
        <div>
            <?php if (!empty($device->id)): ?>
                <a class="button" href="<?=Flight::util()::action('@delete', ['id' => htmlspecialchars($device->id)])?>" onclick="return confirm('Wollen Sie dieses Gerät wirklich löschen?');">Löschen</a>
            <?php endif; ?>
        </div>

        <div>
            <button class="button"
                    type="reset">Zurücksetzen</button>
            <button class="button primary"
                    type="submit">Speichern</button>
        </div>
    </footer>
</form>
