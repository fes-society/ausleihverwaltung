<header>
    <div></div>
    <div>
        <?php if (!empty($query)): ?>
        <span><?=Flight::util()::quantize('%s Suchergebnis', '%s Suchergebnisse', $amount_result)?></span>
        <a class="button"
           href="<?=Flight::util()::action('@index')?>">Suche zurücksetzen</a>
        <?php endif; ?>
        <a href="<?= Flight::util()::action('@create') ?>" class="button">Neues Gerät erstellen</a>
    </div>
</header>

<form class="search-form">
    <label for="query"
           class="query">
        <div class="title">Suchen</div>
        <input name="query"
               id="query"
               value="<?=htmlspecialchars($query)?>"/>
    </label>
    <button class="button" type="submit">Suchen</button>
</form>

<?=PaginationController::show(Flight::util()::action('@index'), $amount_result, count($devices))?>

<table class="table">
    <thead>
    <tr>
        <th>Inventarnr.</th>
        <th>Gerätetyp</th>
        <th>Zustand</th>
        <th>Hersteller</th>
        <th>Modell</th>
        <th>Kaufdatum</th>
        <th>Garantiedauer</th>
        <th>Lagerort</th>
        <th>Kommentar</th>
        <th>Zubehör</th>
        <th class="Vertrauenslevel">Vertrauen</th>
        <th></th>
    </tr>
    </thead>
    <tbody>
    <?php foreach ($devices as $device): ?>
        <tr>
            <td><?=htmlspecialchars($device->inventory_number)?></td>
            <td><?=Flight::util()::model('DeviceType')->find($device->device_type_id)->title?></td>
            <td><?=htmlspecialchars($device->condition)?></td>
            <td><?=htmlspecialchars($device->manufacturer)?></td>
            <td><?=htmlspecialchars($device->model)?></td>
            <td><?=htmlspecialchars($device->get_buy_date())?></td>
            <td><?=htmlspecialchars($device->guarantee_duration)?></td>
            <td><?=htmlspecialchars($device->storage_location)?></td>
            <td><?=htmlspecialchars($device->comment)?></td>
            <td><?=htmlspecialchars($device->equipment)?></td>
            <td><?=htmlspecialchars($device->trust_level)?></td>
            <td><a href="<?= Flight::util()::action('@edit', ['id' => htmlspecialchars($device->id)]) ?>">Bearbeiten</a></td>
        </tr>
    <?php endforeach; ?>
    </tbody>
</table>

<?=PaginationController::show(Flight::util()::action('@index'), $amount_result, count($devices))?>

