<?php if ($is_created): ?>

<fieldset class="meta-info">
    <legend><?=htmlspecialchars($title)?></legend>
    <?php if ($updated != $created): ?>
        <p>Zuletzt Aktualisiert von <a href="<?=htmlspecialchars($updater_url)?>"><?=htmlspecialchars($updater->name())?></a> am <?=$updated?></p>
    <?php endif; ?>
    <p>Erstellt von <a href="<?=htmlspecialchars($creator_url)?>"><?=htmlspecialchars($creator->name())?></a> am <?=$created?></p>
</fieldset>

<?php endif; ?>
