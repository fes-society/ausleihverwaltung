<form method="post" class="login">
    <label for="username">
         <div class="title">Benutzername</div>
         <input name="username" id="username" autofocus required />
    </label>
    <label for="password">
         <div class="title">Passwort</div>
         <input name="password" id="password" type="password" required />
    </label>

    <button type="submit" class="button primary">Login</button>
</form>

<span id="delayed-badge">0</span>
