<?php if ($tempory_storage->has('alert')):
    $alert = $tempory_storage->get('alert') ?>
    <div id="main-alert" class="alert <?=$alert['type']?>">
        <?php
            $alerts = explode("\n", $alert['message']);
            foreach ($alerts as $alert) {
                echo htmlspecialchars($alert);
                echo '<br />';
            }
        ?>
        <button onclick="getElementById('main-alert').style.display = 'none'">
            X
        </button>
    </div>
<?php endif; ?>
