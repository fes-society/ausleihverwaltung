<?php if (Flight::session()->is_logged_in()): ?>
    <header>
        <nav>
            <?php if (Flight::session()->is_admin() || Flight::session()->is_agent()): ?>
                <a href="<?=Flight::util()::action('RentalProcessController@active')?>" id="delayed-badge">
                    Ein überzogener Verleihprozess
                </a>
            <?php
                $active_processes_title = 'Aktive Verleihe';
            else:
                $active_processes_title = 'Übersicht';
            endif; ?>

            <?=Flight::util()::router_link('RentalProcessController@active', $active_processes_title)?>

            <?php if (Flight::session()->is_admin()): ?>
                <?=Flight::util()::router_link('RentalProcessController@history', 'Verleihhistorie')?>
                <?=Flight::util()::router_link('CustomersController@index', 'Kunden')?>
                <?=Flight::util()::router_link('DeviceController@index', 'Geräte')?>
                <?=Flight::util()::router_link('DeviceTypeController@index', 'Gerätetypen')?>
            <?php elseif (Flight::session()->is_agent()): ?>
                <?=Flight::util()::router_link('CustomersController@index', 'Kunden')?>
            <?php endif; ?>

            <?=Flight::util()::router_link('NewRentalProcessController@select_customer', 'Neuer Verleih', ['button', 'primary'])?>
        </nav>

        <nav>
            <?=Flight::util()::router_link('/me', htmlspecialchars(Flight::session()->user()->name()))?>
            <?=Flight::util()::router_link('AuthController@logout', 'Logout')?>
        </nav>
    </header>
<?php endif; ?>
