.mode csv
.header off
.import migration/test-data/customers.csv customers
.import migration/test-data/devices.csv devices
.import migration/test-data/rentalprocess.csv rentalprocess
.import migration/test-data/devicetypes.csv devicetypes
