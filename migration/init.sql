CREATE TABLE `DeviceTypes` (
	`id`	INTEGER PRIMARY KEY AUTOINCREMENT,
	`title`	TEXT NOT NULL,
	`description`	TEXT
);

CREATE TABLE IF NOT EXISTS "RentalProcess" (
	`id`	INTEGER PRIMARY KEY AUTOINCREMENT,
	`device_id`	INTEGER NOT NULL,
	`customer_id`	INTEGER NOT NULL,
	`start_time`	INTEGER NOT NULL,
	`end_time`	INTEGER NOT NULL,
	`returned_at`	INTEGER NOT NULL,
	`returned_by`	INTEGER NOT NULL,
	`collected_at`	INTEGER NOT NULL,
	`collected_by`	INTEGER NOT NULL,
	`created_at`	INTEGER NOT NULL,
	`created_by`	INTEGER NOT NULL
);

CREATE TABLE IF NOT EXISTS "Customers" (
	`id`	INTEGER PRIMARY KEY AUTOINCREMENT,
	`firstname`	TEXT,
	`lastname`	TEXT,
	`identity_card_number`	TEXT,
	`date_of_birth`	TEXT,
	`street`	TEXT,
	`street_number`	TEXT,
	`postcode`	TEXT,
	`place`	TEXT,
	`email`	TEXT,
	`phone_number`	TEXT,
	`phone_number_legal_guardian`	TEXT,
	`accepted_external_use_conditions`	INTEGER,
	`trust_level`	INTEGER NOT NULL,
	`username`	TEXT,
	`password`	TEXT,
	`role`	TEXT NOT NULL,
	`updated_at`	INTEGER NOT NULL,
	`updated_by`	INTEGER NOT NULL,
	`created_at`	INTEGER NOT NULL,
	`created_by`	INTEGER NOT NULL
);

CREATE TABLE IF NOT EXISTS "Devices" (
	`id`	INTEGER PRIMARY KEY AUTOINCREMENT,
	`inventory_number`	INTEGER NOT NULL UNIQUE,
	`device_type_id`	INTEGER,
	`condition`	TEXT,
	`manufacturer`	TEXT,
	`model`	TEXT,
	`buy_date`	TEXT,
	`guarantee_duration`	TEXT,
	`storage_location`	TEXT,
	`comment`	TEXT,
	`equipment`	TEXT,
	`trust_level`	INTEGER,
	`updated_at`	INTEGER NOT NULL,
	`updated_by`	INTEGER NOT NULL,
	`created_at`	INTEGER NOT NULL,
	`created_by`	INTEGER NOT NULL
);

.read migration/insert-admin.sql

.read migration/migrate-all.sql
