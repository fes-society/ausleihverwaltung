# Geräteverleih

## Framework

It uses the [PHP Flight Framework](https://github.com/mikecao/flight). To correctly use the routes
you may need to write the following in a .htaccess file inside the `web/` directory:

```
RewriteEngine On
RewriteCond %{REQUEST_FILENAME} !-f
RewriteCond %{REQUEST_FILENAME} !-d
RewriteRule ^(.*)$ index.php [QSA,L]
```

## Database

You also need to set up the sqlite3 database `db.sqlite`.

Use this to create the database (it calls all migration scripts): `sqlite3 db.sqlite < migration/init.sql`

And optionially fill the database with some test data: `sqlite3 db.sqlite < migration/fill-data.sql`

### Migration

1. Add a .sql file to the `migration/data/` directory
2. Use a format like the previous files: the current date and a short description what this file does
3. Add `.read migration/data/x.sql` to `migration/all-migrations.sql` for future initial database setups
4. Run your file like `sqlite3 db.sqlite < migration/data/x.sql`

Make sure you don't run migrations more than once.
