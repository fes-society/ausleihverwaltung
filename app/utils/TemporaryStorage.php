<?php

class TemporaryStorage {

    const COOKIE_KEY = 'TEMPSTORAGE';

    private $storage;

    function __construct () {
        $this->load();
    }

    /**
     * Stores a new value
     *
     * @param string $key The key for the storage
     * @param string|array $value The actual value
     */
    function store($key, $value) {
        $this->storage[$key] = $value;
        $this->save();
    }

    /**
     * Checks for the existents of the key
     *
     * @param string $key The key for the storage
     */
    function has($key) {
        return array_key_exists($key, $this->storage);
    }

    /**
     * Removes the key and value
     *
     * @param string $key The key for the storage
     */
    function remove($key) {
        unset($this->storage[$key]);
        $this->save();
    }

    /**
     * Gets the value of the given key and removes the value from the storage if not said otherwise
     *
     * @param string $key The key for the storage
     * @param boolean $keep If the value should stay in the storage
     */
    function get($key, $keep = false) {
        if (!$this->has($key))
            throw new Exception('The Key '.$key.' does not exist in the TemporaryStorage');

        $result = $this->storage[$key];

        if (!$keep)
            $this->remove($key);

        return $result;
    }

    private function save() {
        setcookie(self::COOKIE_KEY, json_encode($this->storage), 0, '/', '', false, true);
    }

    private function load() {
        $this->storage = json_decode($_COOKIE[self::COOKIE_KEY] ?? '[]', true);
        if (!is_array($this->storage)) {
            $this->storage = [];
        }
    }

}
