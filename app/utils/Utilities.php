<?php

class Utilities {

    /**
     * Return a new instance of given class and inject the database to the model constructor.
     */
    static function model($class)
    {
        return new $class(Flight::db());
    }

    /**
     * Store a failure alert for the next response in the temporary storage
     */
    static function failure($message)
    {
        Flight::log()::debug('failure: '.$message);
        Flight::temp()->store('alert', [
            'type' => 'failure',
            'message' => self::unmask($message)
        ]);
    }

    /**
     * Store a success alert for the next response in the temporary storage
     */
    static function success($message)
    {
        Flight::log()::debug('success: '.$message);
        Flight::temp()->store('alert', [
            'type' => 'success',
            'message' => self::unmask($message)
        ]);
    }

    /**
     * Navigate back to the last route
     */
    static function back()
    {
        $request = Flight::request();
        $is_referrer_same_as_current = strpos($request->referrer, $request->url) !== false;
        if (!$request->referrer || ($is_referrer_same_as_current && $request->method == 'GET')) {
            $redirect_to_base = true;
        }
        Flight::redirect((isset($redirect_to_base)) ? '/' : $request->referrer);
        exit();
    }

    /**
     * Check if given parameter is a valid action in the format [controller]@method
     */
    static function is_valid_action($action)
    {
        return preg_match('/.*@.+/', $action);
    }

    /**
     * Builds an url for given controller and function (optionally with paramaters).
     * When only a function given, it will use the controller of the current route.
     *
     * @param string $action Controller@function / @function
     * @param array $parameters Indexed list of parameters
     * @return string The parsed url
     */
    static function action($action, $parameters = []) {
        if (!self::is_valid_action($action)) {
            return $action;
        }

        $controller = explode('@', $action)[0];
        $function = explode('@', $action)[1];

        if (empty($controller)) {
            if (!Flight::router()->current() || !is_array(Flight::router()->current()->callback))
                throw new \Exception(sprintf('Couldn\'t get route for @%s, no current controller found', $function));

            $controller = Flight::router()->current()->callback[0];
        }

        foreach (Flight::router()->getRoutes() as $route) {
            if (!is_array($route->callback) ||
                $route->callback[0] != $controller ||
                $route->callback[1] != $function) {
                continue;
            }

            $url = $route->pattern;
            foreach ($parameters as $key => $value) {
                $url = str_replace('@'.$key, $value, $url);
            }
            return $url;
        }

        throw new \Exception(sprintf('No route found for %s@%s', $controller, $function));
    }

    /**
     * If valid action given, redirect can use the action as destination
     */
    static function before_redirect(&$params, &$output) {
        if (self::is_valid_action($params[0])) {
            $params[0] = self::action($params[0]);
        }
    }

    /**
     * Generates an anchor tag with given url, title and classes.
     * Additionally sets an active class when active url matches given path.
     */
    static function router_link($action, $title = null, $classes = []) {
        if (is_array($action)) {
            $path = $action[0];
            $parameters = $action[1];
        } else {
            $path = $action;
            $parameters = [];
        }
        if (self::is_valid_action($path)) {
            $path = self::action($path, $parameters);
        }
        $title = $title ?? $path;
        if (strpos(Flight::request()->url, $path) === 0) {
            $classes[] = 'active';
        }
        $classes = implode($classes, ' ');
        return '<a href="'.htmlspecialchars($path).'" class="'.htmlspecialchars($classes).'">'.htmlspecialchars($title).'</a>';
    }

    /**
     * Generates CSS classes for body based on current controller and method
     */
    static function route_body_class()
    {
        if (!is_array(Flight::router()->current()->callback)) {
            return '';
        }
        $callback = Flight::router()->current()->callback;
        $controller = str_replace('Controller', '', $callback[0]);
        return strtolower($controller).' '.$callback[1];
    }

    /**
     * Mask strings which are in surrounded by {mask}...{/mask}.
     */
    static function mask($text)
    {
        return preg_replace('/{mask}.*{\/mask}/', '{masked}', $text);
    }

    /**
     * Gibt gegebene Sekunden in dem nächstbesten Format aus (Jahr, Monat, Woche, Tage, Stunden, Minuten oder Sekunden).
     *
     * @param Number Sekunden die berechnet werden sollen
     * @param Boolean Ob ein 'Vor' oder 'In' voran gestellt werden soll
     */
    static function dume($timestamp, $with_prefix = false)
    {
        $time_definitions = [
            'second' => 1,
            'minute' => 60,
            'hour' => 60 * 60,
            'day' => 24 * 60 * 60,
            'week' => 7 * 24 * 60 * 60,
            'month' => 30 * 24 * 60 * 60,
            'year' => 365 * 24 * 60 * 60
        ];

        $label_singular = [
            'year' => 'Jahr',
            'month' => 'Monat',
            'week' => 'Woche',
            'day' => 'Tag',
            'hour' => 'Stunde',
            'minute' => 'Minute',
            'second' => 'Sekunde'
        ];

        $label_plural = [
            'year' => 'Jahre',
            'month' => 'Monate',
            'week' => 'Wochen',
            'day' => 'Tage',
            'hour' => 'Stunden',
            'minute' => 'Minuten',
            'second' => 'Sekunden'
        ];

        $label_plural_with_prefix = [
            'year' => 'Jahren',
            'month' => 'Monaten',
            'week' => 'Wochen',
            'day' => 'Tagen',
            'hour' => 'Stunden',
            'minute' => 'Minuten',
            'second' => 'Sekunden'
        ];

        $time = ($timestamp < 0) ? $timestamp * -1 : $timestamp;

        if ($time >= $time_definitions['year']) {
            $type = 'year';
        } else if ($time >= $time_definitions['month']) {
            $type = 'month';
        } else if ($time >= $time_definitions['week']) {
            $type = 'week';
        } else if ($time >= $time_definitions['day']) {
            $type = 'day';
        } else if ($time >= $time_definitions['hour']) {
            $type = 'hour';
        } else if ($time >= $time_definitions['minute']) {
            $type = 'minute';
        } else {
            $type = 'second';
        }

        if ($with_prefix) {
            if ($timestamp == 0) return 'jetzt';
            $prefix = ($timestamp < 0) ? 'vor ' : 'in ';
            return $prefix.self::pluralize(round($time / $time_definitions[$type]), $label_singular[$type], $label_plural_with_prefix[$type]);
        } else {
            return self::pluralize(round($time / $time_definitions[$type]), $label_singular[$type], $label_plural[$type]);
        }
    }

    static function pluralize($number, $singular, $plural)
    {
        return $number.' '.(($number == 1) ? $singular : $plural);
    }

    static function quantize($singular, $plural, $number)
    {
        return sprintf(($number == 1) ? $singular : $plural, $number);
    }

    /**
     * Renders the content view inside the layout view.
     */
    static function render($file, $data = null)
    {
        if (is_array($data)) {
            $title = $data['title'] ?? null;
        } else {
            $title = $data;
        }
        Flight::render($file, $data, 'content');
        Flight::render('layout', [
            'title' => $title ?? 'No title'
        ]);
    }

    /**
     * Unmask a string, which means that the keyword {mask} and {/mask} are removed.
     */
    static function unmask($text)
    {
        $text = str_replace('{mask}', '', $text);
        $text = str_replace('{/mask}', '', $text);
        return $text;
    }

    static function internal_server_error($error)
    {
        Flight::log()::error($error);
        echo '<h1>500 Internal Server Error</h1>';
        echo '<p>Something went wrong... Try again later.</p>';
        echo '<p>'.(new DateTime())->format('d.m.Y H:i:s.v').'</p>';
        exit();
    }

    static function pagination_helper()
    {
        $page = ((int) Flight::request()->query['page'] ?? 0);
        $limit = Flight::get('app.results.default_limit');
        $offset = $page * $limit;

        return [
            'limit' => $limit,
            'offset' => $offset
        ];
    }

}
