<?php

class Logger {

    const LEVEL_TITLE = [
        'debug' => 'DEBUG',
        'info' => 'INFO ',
        'error' => 'ERROR'
    ];
    const LEVELS = [
        'debug' => 2,
        'info' => 1,
        'error' => 0
    ];
    const TIME_FORMAT = 'Y-m-d H:i:s.v';
    const FILE = '../app.log';
    const ERROR_LOG_TYPE = 3;
    const SESSION_HASH_LENGTH = 6;
    const NEWLINE = "\n";

    /**
     * Log debug, info and error messages. Sensitive information will be masked.
     *
     * Example:
     * 2018-08-18 01:22:16.520 DEBUG /customers/9/ af8db2 username[2] success alert: Deleted customer {masked} (Nr. 9) successfully
     * |^^^^^^^^^ |^^^^^^^^^^^ |^^^^ |^^^^^^^^^^^  |^^^^^ |^^^^^^^^^^ ^^^^^^^^^^|^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
     * date       time         level path          |      logged in user        message
     *                                             trimmed hash of session id
     *
     * @param String Message
     * @param String Log level
     */
    private static function log($message, $level)
    {
        if (self::LEVELS[Flight::get('app.log.level')] < self::LEVELS[$level]) {
            return;
        }

        $row[] = (new DateTime())->format(self::TIME_FORMAT);
        $row[] = self::LEVEL_TITLE[$level];
        $row[] = Flight::request()->url;
        $row[] = substr(hash('sha256', session_id()), 0, self::SESSION_HASH_LENGTH);
        $user = Flight::session()->user();
        if (isset($user)) {
            $row[] = $user->username.'['.$user->id.']';
        }
        $row[] = Flight::util()::mask($message);

        error_log(implode($row, ' ').self::NEWLINE, self::ERROR_LOG_TYPE, self::FILE);
    }

    static function debug($message)
    {
        self::log($message, 'debug');
    }

    static function info($message)
    {
        self::log($message, 'info');
    }

    static function error($message)
    {
        self::log($message, 'error');
    }

}
