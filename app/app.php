<?php

/**
 * Require all utils
 */
require 'utils/Log.php';
require 'utils/Kiwi.php';
require 'utils/KiwiMeta.php';
require 'utils/Utilities.php';
require 'utils/TemporaryStorage.php';

/**
 * Require all models, most of them are an active record of a database table
 */
require 'models/Session.php';
require 'models/Customer.php';
require 'models/Device.php';
require 'models/DeviceType.php';
require 'models/NewRentalProcess.php';
require 'models/RentalProcess.php';

/**
 * Require all controllers
 */
require 'controllers/AuthController.php';
require 'controllers/DetailMetaController.php';
require 'controllers/PaginationController.php';
require 'controllers/CustomersController.php';
require 'controllers/DeviceController.php';
require 'controllers/DeviceTypeController.php';
require 'controllers/NewRentalProcessController.php';
require 'controllers/RentalProcessController.php';

/**
 * Require routes, which map paths to a static method of a controller
 */
require 'routes.php';

/**
 * Register global class instances with Flight
 */
Flight::register('db', 'SQLite3', ['../db.sqlite']);
Flight::register('util', 'Utilities');
Flight::register('temp', 'TemporaryStorage');
Flight::register('log', 'Logger');
Flight::register('session', 'Session');

/**
 * Declare global Flight variables
 */
Flight::set('app.log.level', 'debug');
Flight::set('app.title', 'Ausleihverwaltung');
Flight::set('app.results.default_limit', 100);

/**
 * Extend existing methods of the Flight framework
 */
Flight::map('error', ['Utilities', 'internal_server_error']);
Flight::before('redirect', ['Utilities', 'before_redirect']);
