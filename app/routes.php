<?php

/* AuthController */
Flight::route('*', ['AuthController', 'middleware']);
Flight::route('GET /login', ['AuthController', 'login_form']);
Flight::route('POST /login', ['AuthController', 'login']);
Flight::route('GET /logout', ['AuthController', 'logout']);

/* DeviceTypeController */
Flight::route('/types/*', ['AuthController', 'middleware_only_admin']);
Flight::route('GET /types', ['DeviceTypeController', 'index']);
Flight::route('GET /types/create', ['DeviceTypeController', 'create']);
Flight::route('POST /types/create', ['DeviceTypeController', 'store']);
Flight::route('GET /types/@id', ['DeviceTypeController', 'edit']);
Flight::route('POST /types/@id', ['DeviceTypeController', 'update']);
Flight::route('GET /types/@id/delete', ['DeviceTypeController', 'delete']);

/* RentalProcessController */
Flight::route('GET /history', ['AuthController', 'middleware_only_admin']);
Flight::route('GET /history', ['RentalProcessController', 'history']);

Flight::route('GET /delayed', ['RentalProcessController', 'delayed']);
Flight::route('GET /overview', ['RentalProcessController', 'active']);
Flight::route('GET /rental-process/return/@id', ['RentalProcessController', 'return']);
Flight::route('GET /rental-process/collect/@id', ['RentalProcessController', 'collect']);
Flight::route('GET /rental-process/delete/@id', ['RentalProcessController', 'delete']);
Flight::route('/rental-process/reset', ['AuthController', 'middleware_only_admin']);
Flight::route('POST /rental-process/reset', ['RentalProcessController', 'reset']);

/* NewRentalProcessController */
Flight::route('GET /new-rental-process', ['NewRentalProcessController', 'select_customer']);
Flight::route('POST /new-rental-process', ['NewRentalProcessController', 'store_customer']);
Flight::route('GET /new-rental-process/time', ['NewRentalProcessController', 'select_time']);
Flight::route('POST /new-rental-process/time', ['NewRentalProcessController', 'store_time']);
Flight::route('GET /new-rental-process/device', ['NewRentalProcessController', 'select_device']);
Flight::route('POST /new-rental-process/device', ['NewRentalProcessController', 'store_device']);
Flight::route('GET /new-rental-process/device/@id/remove', ['NewRentalProcessController', 'remove_device']);
Flight::route('GET /new-rental-process/comment', ['NewRentalProcessController', 'write_comment']);
Flight::route('POST /new-rental-process/comment', ['NewRentalProcessController', 'store_comment']);
Flight::route('GET /new-rental-process/abort', ['NewRentalProcessController', 'abort']);
Flight::route('GET /new-rental-process/store', ['NewRentalProcessController', 'store']);

/* DeviceController */
Flight::route('/devices/*', ['AuthController', 'middleware_only_admin']);
Flight::route('POST /devices/create', ['DeviceController', 'store']);
Flight::route('GET /devices/create', ['DeviceController', 'create']);
Flight::route('GET /devices', ['DeviceController', 'index']);
Flight::route('GET /devices/@id', ['DeviceController', 'edit']);
Flight::route('POST /devices/@id', ['DeviceController', 'update']);
Flight::route('GET /devices/@id/delete', ['DeviceController', 'delete']);

/* CustomersController */
Flight::route('/customers/*', ['AuthController', 'middleware_at_least_agent']);
Flight::route('GET /customers', ['CustomersController', 'index']);
Flight::route('GET /customers/create', ['CustomersController', 'create']);
Flight::route('POST /customers/create', ['CustomersController', 'store']);
Flight::route('GET /customers/@id', ['CustomersController', 'edit']);
Flight::route('POST /customers/@id', ['CustomersController', 'update']);
Flight::route('/customers/@id/delete', ['AuthController', 'middleware_only_admin']);
Flight::route('GET /customers/@id/delete', ['CustomersController', 'delete']);

Flight::route('GET /me', ['CustomersController', 'edit']);
Flight::route('POST /me', ['CustomersController', 'update']);

Flight::route('/', function() {
    Flight::redirect('RentalProcessController@active');
});

Flight::route('*', function() {
    Flight::util()::render('not-found', '404 - Seite nicht gefunden');
});
