<?php

class DeviceController {

    const TITLE = 'Gerät';
    const SUCCESS_MESSAGE = 'Gerät %s erfolgreich %s';

    /**
     * Bietet eine Übersichtstabelle von allen Geräten mit einer Suche
     */
    static function index()
    {
        try {
            $devices = Flight::util()::model('Device');

            if (!empty(Flight::request()->query['query'])) {
                $query = Flight::request()->query['query'];
                $devices->where('inventory_number LIKE ', '%'.$query.'%')
                    ->where('OR condition LIKE ', '%'.$query.'%')
                    ->where('OR manufacturer LIKE ', '%'.$query.'%')
                    ->where('OR model LIKE ', '%'.$query.'%')
                    ->where('OR buy_date LIKE ', '%'.$query.'%')
                    ->where('OR guarantee_duration LIKE ', '%'.$query.'%')
                    ->where('OR storage_location LIKE ', '%'.$query.'%')
                    ->where('OR comment LIKE ', '%'.$query.'%')
                    ->where('OR equipment LIKE ', '%'.$query.'%')
                    ->where('OR trust_level = ', $query);

                $device_types = Flight::util()::model('DeviceType')
                    ->where('title LIKE ', '%'.$query.'%')
                    ->all();

                $device_type_ids = array_map(function($type) {
                    return $type->id;
                }, $device_types);
                $devices->where('OR device_type_id IN ('.implode($device_type_ids, ', ').')');
            }

            $all_types = Flight::util()::model('DeviceType')->all();

            $pagination = Flight::util()::pagination_helper();
            $amount_result = $devices->count();
            $result = $devices
                ->limit($pagination['limit'], $pagination['offset'])
                ->all();

            Flight::util()::render('devices/overview', [
                'devices' => $result,
                'amount_result' => $amount_result,
                'query' => $query ?? '',
                'query_device_type_id' => $query_device_type_id ?? '',
                'all_types' => $all_types,
                'title' => 'Geräteverwaltung'
            ]);
        } catch (\Exception $e) {
            self::handle_error($e);
        }
    }

    /**
     * Speichert ein neues Gerät in der Datenbank und leitet anschließend zur Geräte Übersicht
     */
    static function store()
    {
        try {
            $device = Flight::util()::model('Device')
                ->fill(Flight::request()->data)
                ->create_as(Flight::session()->user()->id);
            Flight::util()::success(sprintf(self::SUCCESS_MESSAGE, $device->inventory_number, 'erstellt'));
            Flight::redirect(Flight::util()::action('@edit', ['id' => $device->id]));
        } catch (\Exception $e) {
            self::handle_error($e);
        }
    }

    /**
     * Bietet ein Erstellformular für ein neues Gerät
     */
    static function create()
    {
        try {
            $device = Flight::util()::model('Device');
            $all_types = Flight::util()::model('DeviceType')->all();

            Flight::util()::render('devices/detail', [
                'device' => $device,
                'all_types' => $all_types,
                'title' => 'Neues '.self::TITLE
            ]);
        } catch (\Exception $e) {
            self::handle_error($e);
        }
    }

    /**
     * Überschreibt das Gerät mit gegebener Id mit neuen Daten und leitet anschließend zur Detailseite
     * @param Any Id
     */
    static function update($id)
    {
        try {
            $device = Flight::util()::model('Device')
                ->find_or_fail($id)
                ->fill(Flight::request()->data)
                ->update_as(Flight::session()->user()->id);
            Flight::util()::success(sprintf(self::SUCCESS_MESSAGE, $device->inventory_number, 'geändert'));
            Flight::util()::back();
        } catch (\Exception $e) {
            self::handle_error($e);
        }
    }

    /**
     * Bietet ein Bearbeitungsformular für ein bestehendes Gerät mit gegebener Id
     * @param Any Id
     */
    static function edit($id)
    {
        try {
            $device = Flight::util()::model('Device')->find_or_fail($id);
            $all_types = Flight::util()::model('DeviceType')->all();

            Flight::util()::render('devices/detail', [
                'device' => $device,
                'all_types' => $all_types,
                'title' => self::TITLE.' '.$device->inventory_number
            ]);
        } catch (\Exception $e) {
            self::handle_error($e);
        }
    }

    /**
     * Löscht das Gerät mit angegebener Nummer und leitet dann zur Übersicht weiter
     * @param Any Id
     */
    static function delete($id)
    {
        try {
            $device = Flight::util()::model('Device')
                ->find_or_fail($id)
                ->delete();
            Flight::util()::success(sprintf(self::SUCCESS_MESSAGE, $device->inventory_number, 'gelöscht'));
            Flight::redirect('@index');
        } catch (\Exception $e) {
            self::handle_error($e);
        }
    }

    /**
     * Im Falle eines 404 Errors leitet der Error Handler in jedem Fall zur Übersicht
     * ansonsten wird zurückgeleitet mit der Message aus der Exception
     * @param \Exception
     */
    private static function handle_error($error)
    {
        if ($error->getCode() == 404) {
            Flight::util()::failure(self::TITLE.' nicht gefunden');
            Flight::redirect('@index');
        } else {
            Flight::util()::failure($error->getMessage());
            Flight::util()::back();
        }
    }
}
