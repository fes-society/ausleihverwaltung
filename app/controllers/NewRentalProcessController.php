<?php

class NewRentalProcessController {

    const STORAGE_KEY = 'new_rental_process';

    static function select_customer()
    {
        self::render_new_process('select-customer', 'Kunden auswählen');
    }

    static function store_customer()
    {
        try {
            if (empty(Flight::request()->data->customer_number)) {
                throw new \Exception('Bitte geben Sie eine Kundennummer an');
            }

            $process = self::new_rental_process_from_storage();

            $customer = self::model_customer()
                ->find_or_fail(Flight::request()->data->customer_number);

            $process->customer_id = $customer->id;
            Flight::temp()->store(self::STORAGE_KEY, $process);

            Flight::redirect('@select_time');
        } catch (\Exception $e) {
            if ($e->getCode() == 404) {
                Flight::util()::failure('Mit der Kundennummer '.Flight::request()->data->customer_number.' wurde kein Kunde gefunden');
                Flight::redirect('@select_customer');
            } else {
                self::handle_error($e);
            }
        }
    }

    static function select_time()
    {
        try {
            $process = self::new_rental_process_from_storage();

            $data['start_date'] = (new DateTime($process->start_time ?? 'now'))->format('Y-m-d');
            $data['start_time'] = (new DateTime($process->start_time ?? 'now'))->format('H:i');
            $data['end_date'] = (new DateTime($process->end_time ?? 'now'))->format('Y-m-d');
            $data['end_time'] = (new DateTime($process->end_time ?? 'now'))->format('H:i');

            self::render_new_process('select-time', 'Zeitraum auswählen', $data);
        } catch (\Exception $e) {
            self::handle_error($e);
        }
    }

    static function store_time()
    {
        try {
            $process = self::new_rental_process_from_storage();

            $process->set_start_time(Flight::request()->data->start_date.' '.Flight::request()->data->start_time);
            $process->set_end_time(Flight::request()->data->end_date.' '.Flight::request()->data->end_time);
            Flight::temp()->store(self::STORAGE_KEY, $process);

            Flight::redirect('@select_device');
        } catch (\Exception $e) {
            self::handle_error($e);
        }
    }

    static function select_device()
    {
        if (!empty(Flight::request()->query['query_device_type_id'])) {
            $new_process = self::new_rental_process_from_storage();

            $query_device_type_id = Flight::request()->query['query_device_type_id'];

            $devices = self::model_device()
                ->where('device_type_id = ', $query_device_type_id)
                ->all();

            $devices_filtered = array_filter($devices, function ($device) use ($new_process) {
                return !$device->is_available() && array_search($device->id, $new_process->device_ids) === false;
            });
        }

        $all_types = Flight::util()::model('DeviceType')->all();

        self::render_new_process('select-device', 'Geräte auswählen', [
            'devices' => $devices_filtered ?? [],
            'all_types' => $all_types,
            'query_device_type_id' => $query_device_type_id ?? ''
        ]);
    }

    static function store_device()
    {
        try {
            $new_process = self::new_rental_process_from_storage();
            $inventory_number = Flight::request()->data->inventory_number;

            $device = self::model_device()
                ->where('inventory_number = ', (int) $inventory_number)
                ->first();

            if (empty($device->id)) {
                Flight::util()::failure(sprintf('Es wurde kein Gerät mit der Inventar Nr. %s gefunden', $inventory_number));
                Flight::util()::back();
            }

            $customer = Flight::util()::model('Customer')
                ->find($new_process->customer_id);
            if ($customer->trust_level < $device->trust_level) {
                Flight::util()::failure(sprintf('Das Vertrauenslevel vom Kunden ist %s Punkte zu niedrig um Gerät Nr. %s auszuleihen.',
                    $device->trust_level - $customer->trust_level,
                    $inventory_number
                ));
                Flight::util()::back();
            }

            if (array_search($device->id, $new_process->device_ids) !== false) {
                Flight::util()::failure(sprintf('Gerät "%s" Nr. %s wurde bereits hinzugefügt',
                    $device->type()->title,
                    $device->inventory_number
                ));
                Flight::util()::back();
            }

            $all_process = self::model_process()
                ->where('returned_at = ', 0)
                ->where('AND device_id = ', $device->id)
                ->all();

            foreach ($all_process as $process) {
                if ($process->is_collected()) {
                    Flight::util()::failure(sprintf('Gerät "%s" Nr. %s ist zurzeit nicht im Lager und ist noch von %s bis %s (Endet %s) ausgeliehen.',
                        $device->type()->title,
                        $device->inventory_number,
                        $process->get_start_time(),
                        $process->get_end_time(),
                        Flight::util()::dume($process->end_time - time(), true)
                    ));
                    Flight::util()::back();
                }

                if ($process->is_overlapping_with((new DateTime($new_process->start_time))->getTimestamp(), (new DateTime($new_process->end_time))->getTimestamp())) {
                    Flight::util()::failure(sprintf('Gerät "%s" Nr. %s wird von %s bis %s (Endet %s) bereits ausgeliehen.',
                        $device->type()->title,
                        $device->inventory_number,
                        $process->get_start_time(),
                        $process->get_end_time(),
                        Flight::util()::dume($process->end_time - time(), true)
                    ));
                    Flight::util()::back();
                }
            }

            $new_process->device_ids[] = $device->id;
            Flight::temp()->store(self::STORAGE_KEY, $new_process);
            Flight::util()::back();
        } catch (\Exception $e) {
            self::handle_error($e);
        }
    }

    static function remove_device($device_id)
    {
        try {
            $process = self::new_rental_process_from_storage();
            $process->device_ids = array_diff($process->device_ids, [$device_id]);
            Flight::temp()->store(self::STORAGE_KEY, $process);
            Flight::redirect('@select_device');
        } catch (\Exception $e) {
            self::handle_error($e);
        }
    }

    static function write_comment()
    {
        try {
            $process = self::new_rental_process_from_storage();
            $data['comment'] = $process->comment;

            self::render_new_process('write-comment', 'Kommentar', $data);
        } catch (\Exception $e) {
            self::handle_error($e);
        }
    }

    static function store_comment()
    {
        try {
            $process = self::new_rental_process_from_storage();
            $process->comment = Flight::request()->data->comment;
            Flight::temp()->store(self::STORAGE_KEY, $process);
            Flight::redirect('@write_comment');
        } catch (\Exception $e) {
            self::handle_error($e);
        }
    }

    static function abort()
    {
        try {
            Flight::temp()->remove(self::STORAGE_KEY);
            Flight::redirect('@select_customer');
        } catch (\Exception $e) {
            self::handle_error($e);
        }
    }

    static function store()
    {
        try {
            $new_process = self::new_rental_process_from_storage();
            $device_ids = array_unique($new_process->device_ids);

            if (empty($new_process->customer_id) || empty($new_process->start_time) || empty($device_ids)) {
                Flight::util()::failure('Es ist ein Fehler aufgetreten. Der Verleih konnte nicht erstellt werden.');
                Flight::redirect('@select_device');
                exit;
            }

            $count = 0;
            foreach ($device_ids as $device_id) {
                $count_active_process_with_same_device = self::model_process()
                    ->where('returned_at = ', 0)
                    ->where('AND collected_at > ', 0)
                    ->where('AND device_id = ', $device_id)
                    ->count();

                if ($count_active_process_with_same_device > 0) {
                    Flight::util()::failure('Der Verleih kann nicht erstellt werden, da ein Gerät bereits verliehen wurde. Das Gerät wurde vom Prozess entfernt...');
                    Flight::redirect(Flight::util()::action('@remove_device', ['id' => $device_id]));
                    exit();
                }

                self::model_process()
                    ->fill([
                        'customer_id' => $new_process->customer_id,
                        'device_id' => $device_id,
                        'start_time' => (new DateTime($new_process->start_time))->getTimestamp(),
                        'end_time' => (new DateTime($new_process->end_time))->getTimestamp(),
                        'comment' => $new_process->comment
                    ])
                    ->create_as(Flight::session()->user()->id);
                $count = $count + 1;
            }

            Flight::util()::success(sprintf('%s für den Zeitraum von %s bis %s erfolgreich erstellt',
                Flight::util()::pluralize($count, 'neuer Verleih', 'neue Verleihe'),
                $new_process->get_start_time(),
                $new_process->get_end_time()
            ));
            Flight::temp()->remove(self::STORAGE_KEY);
            Flight::redirect('/');
        } catch (\Exception $e) {
            self::handle_error($e);
        }
    }

    private static function render_new_process($view, $title, $data = [])
    {
        try {
            Flight::render('new-rental-process/'.$view, $data, 'new_rental_process_content');

            $process = self::new_rental_process_from_storage();
            $customer = self::model_customer()->find($process->customer_id);
            $devices = self::model_device()
                ->where_bulk_id($process->device_ids)
                ->all();
            if (!empty($process->start_time) && !empty($process->end_time)) {
                $time = sprintf('%s – %s (%s)', $process->get_start_time(), $process->get_end_time(), Flight::util()::dume($process->total_time()));
            } else {
                $time = '';
            }

            $active_link = self::get_active_link();
            self::redirect_to_correct_step($active_link, $process, $customer, $time);

            Flight::util()::render('new-rental-process/overview', [
                'active_link' => $active_link,
                'customer' => $customer,
                'time' => $time,
                'devices' => $devices ?? [],
                'comment' => $process->comment,
                'title' => 'Neuer Verleih: '.$title
            ]);
        } catch (\Exception $e) {
            self::handle_error($e);
        }
    }

    private static function redirect_to_correct_step($active_link, $process, $customer, $time)
    {
        if (($active_link == 'time' || $active_link == 'device') && !Flight::session()->is_customer() && (empty($customer->id))) {
            Flight::redirect('@select_customer');
            exit;
        }

        if (($active_link == 'customer' || $active_link == 'device') && empty($time) && !empty($customer->id)) {
            Flight::redirect('@select_time');
            exit;
        }

        if ($active_link != 'comment' && $active_link != 'device' && !empty($process->device_ids)) {
            Flight::redirect('@select_device');
            exit;
        }

        if ($active_link == 'customer' && Flight::session()->is_customer()) {
            Flight::redirect('@select_time');
            exit;
        }
    }

    private static function get_active_link()
    {
        $url = Flight::request()->url;
        if (strpos($url, 'comment')) {
            return 'comment';
        } else if (strpos($url, 'time')) {
            return 'time';
        } else if (strpos($url, 'device')) {
            return 'device';
        } else {
            return 'customer';
        }
    }

    private static function new_rental_process_from_storage()
    {
        $new_process =  new NewRentalProcess((Flight::temp()->has(self::STORAGE_KEY)) ? Flight::temp()->get(self::STORAGE_KEY, true) : []);
        if (Flight::session()->is_customer()) {
            $new_process->customer_id = Flight::session()->user()->id;
        }
        return $new_process;
    }

    private static function handle_error($e)
    {
        Flight::util()::failure($e->getMessage());
        Flight::util()::back();
    }

    private static function model_process()
    {
        return Flight::util()::model('RentalProcess');
    }

    private static function model_customer()
    {
        return Flight::util()::model('Customer');
    }

    private static function model_device()
    {
        return Flight::util()::model('Device');
    }

}
