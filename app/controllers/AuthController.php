<?php

class AuthController {

    private const SESSION_KEY = 'user_id';
    private const LOGIN_ACTION = 'AuthController@login_form';
    private const LOGOUT_ACTION = 'AuthController@logout';

    public static function login_form()
    {
        Flight::util()::render('login', 'Login');
    }

    public static function login()
    {
        try {
            if (empty(trim(Flight::request()->data->username)) || empty(trim(Flight::request()->data->password))) {
                self::login_failed('invalid form');
            }

            $user = Flight::util()::model('Customer')
                ->where('username = ', Flight::request()->data->username)
                ->first();

            $result = self::is_user_valid_for_authentication($user);
            if ($result !== true) {
                self::login_failed($result);
            }

            if (!$user->verify_password(Flight::request()->data->password)) {
                throw new Exception(sprintf('password of %s does not verify', $user->username));
            }

            self::regenerate_session();
            $_SESSION[self::SESSION_KEY] = $user->id;
            Flight::log()::info($user->username.' logged in as '.$user->role);
            Flight::session()->set($user);

            Flight::redirect('/');
        } catch (Exception $e) {
            self::login_failed($e->getMessage());
        }
    }

    public static function logout() {
        if (isset($_SESSION[self::SESSION_KEY])) {
            Flight::log()::info(sprintf('user with id %s logged out', $_SESSION[self::SESSION_KEY]));
            unset($_SESSION[self::SESSION_KEY]);
            self::regenerate_session();

            Flight::temp()->remove('new_rental_process');
        }
        Flight::redirect(self::LOGIN_ACTION);
        exit();
    }

    public static function middleware()
    {
        session_start();

        if (self::is_whitelisted(Flight::request()->url)) {
            return true;
        }

        if (!isset($_SESSION[self::SESSION_KEY])) {
            self::middleware_failed('no session found');
        }

        try {
            $user = Flight::util()::model('Customer')
                ->find($_SESSION[self::SESSION_KEY]);
            $result = self::is_user_valid_for_authentication($user);
            if ($result !== true) {
                self::middleware_failed($result);
            }
            Flight::session()->set($user);
            return true;
        } catch (Exception $e) {
            self::middleware_failed($e->getMessage());
        }
    }

    public static function middleware_at_least_agent()
    {
        if (Flight::session()->is_admin() || Flight::session()->is_agent()) {
            return true;
        }
        Flight::util()::failure('Sie sind nicht berechtigt diese Seite aufzurufen');
        Flight::log()::info('only agent admin authorization middleware failed');
        Flight::util()::back();
    }

    public static function middleware_only_admin()
    {
        if (Flight::session()->is_admin()) {
            return true;
        }
        Flight::util()::failure('Sie sind nicht berechtigt diese Seite aufzurufen');
        Flight::log()::info('only admin authorization middleware failed');
        Flight::util()::back();
    }

    private static function middleware_failed($reason)
    {
        Flight::log()::info('authentication middleware failed - '.$reason);
        Flight::redirect(self::LOGIN_ACTION);
        exit();
    }

    private static function login_failed($reason)
    {
        Flight::log()::info('login failed - '.$reason);
        Flight::util()::failure('Bitte überprüfen Sie Ihre Zugangsdaten');
        Flight::redirect(self::LOGIN_ACTION);
        exit();
    }

    private static function is_user_valid_for_authentication($user)
    {
        if (empty($user->id)) {
            return sprintf('user %s does not exist', Flight::request()->data->username ?? '');
        }
        if (empty($user->username)) {
            sprintf('user with id %s has no username set', $user->id);
        }
        if (empty($user->password)) {
            return sprintf('user with id %s has no password set', $user->id);
        }
        if ($user->role != 'agent' && $user->role != 'admin' && $user->role != 'customer') {
            return sprintf('role of %s is %s', $user, $user->role);
        }
        return true;
    }

    private static function is_whitelisted($url) {
        $whitelist = [
            Flight::util()::action(self::LOGIN_ACTION),
            Flight::util()::action(self::LOGOUT_ACTION),
            Flight::util()::action('RentalProcessController@delayed')
        ];
        return in_array($url, $whitelist);
    }

    private static function regenerate_session()
    {
        if (session_status() == PHP_SESSION_ACTIVE) {
            session_regenerate_id();
        }
    }

}
