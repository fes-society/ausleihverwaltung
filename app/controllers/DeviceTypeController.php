<?php

class DeviceTypeController {

    const TITLE = 'Gerätetyp';
    const SUCCESS_MESSAGE = '%s erfolgreich %s';

    /**
     * Bietet eine Übersichtstabelle von allen Gerätetypen mit einer Suche
     */
    static function index()
    {
        $types = self::model();
        if (!empty(Flight::request()->query['query'])) {
            $query = Flight::request()->query['query'];
            $types->where('title LIKE ', '%'.$query.'%')
                ->where('OR description LIKE ', '%'.$query.'%');
        }

        Flight::util()::render('types/overview', [
            'types' => $types->all(),
            'query' => $query ?? '',
            'title' => 'Gerätetypen'
        ]);
    }

    /**
     * Bietet ein Erstellformular für einen neuen Gerätetyp
     */
    static function create()
    {
        Flight::util()::render('types/detail', [
            'type' => self::model(),
            'title' => 'Neuer '.self::TITLE
        ]);
    }

    /**
     * Speichert einen neuen Gerätetypen der Datenbank und leitet anschließend zur Gerätetyp Übersicht
     */
    static function store()
    {
        try {
            $type = self::model()
                ->fill(Flight::request()->data)
                ->create();
            Flight::util()::success(sprintf(self::SUCCESS_MESSAGE, $type->title, 'erstellt'));
            Flight::redirect('@index');
        } catch (\Exception $e) {
            self::handle_error($e);
        }
    }

    /**
     * Bietet ein Bearbeitungsformular für einen bestehenden Gerätetyp mit gegebener Id
     * @param Any Gerätetypnummer
     */
    static function edit($id)
    {
        try {
            $type = self::model()->find_or_fail($id);

            Flight::util()::render('types/detail', [
                'type' => $type,
                'title' => $type->title
            ]);
        } catch (\Exception $e) {
            self::handle_error($e);
        }
    }

    /**
     * Überschreibt den Gerätetyp mit gegebener Id mit neuen Daten und leitet anschließend zur Detailseite
     * @param Any Gerätetypnummer
     */
    static function update($id)
    {
        try {
            $type = self::model()
                ->find_or_fail($id)
                ->fill(Flight::request()->data)
                ->update();
            Flight::util()::success(sprintf(self::SUCCESS_MESSAGE, $type->title, 'geändert'));
            Flight::redirect('@index');
        } catch (\Exception $e) {
            self::handle_error($e);
        }
    }

    /**
     * Löscht den Gerätetyp mit angegebener Nummer und leitet dann zur Übersicht weiter
     * @param Any Gerätetypnummer
     */
    static function delete($id)
    {
        try {
            $type = self::model()
                ->find_or_fail($id)
                ->delete();
            Flight::util()::success(sprintf(self::SUCCESS_MESSAGE, $type->title, 'gelöscht'));
            Flight::redirect('@index');
        } catch (\Exception $e) {
            self::handle_error($e);
        }
    }

    /**
     * Im Falle eines 404 Errors leitet der Error Handler in jedem Fall zur Übersicht
     * ansonsten wird zurückgeleitet mit der Message aus der Exception
     * @param \Exception
     */
    private static function handle_error($error)
    {
        if ($error->getCode() == 404) {
            Flight::util()::failure(self::TITLE.' nicht gefunden');
            Flight::redirect('@index');
        } else {
            Flight::util()::failure($error->getMessage());
            Flight::util()::back();
        }
    }

    /**
     * Erstellt eine DeviceType Instanz
     * @return DeviceType
     */
    private static function model()
    {
         return Flight::util()::model('DeviceType');
    }
}
