<?php

class DetailMetaController {

    public static $title = 'Info';
    public const TIME_FORMAT = 'd.m.Y \u\m H:i:s';

    public static function show($entity)
    {
        $creator = self::model_user()->find($entity->created_by);

        if ($entity->updated_by == $entity->created_by) {
            $updater = $creator;
        } else {
            $updater = self::model_user()->find($entity->updated_by);
        }

        Flight::render('detail-meta', [
            'is_created' => !empty($creator->id),
            'title' => self::$title,
            'creator' => $creator,
            'creator_url' => Flight::util()::action('CustomersController@edit', ['id' => $creator->id]),
            'created' => date(self::TIME_FORMAT, $entity->created_at).' ('.Flight::util()::dume($entity->created_at - time(), true).')',
            'updater' => $updater,
            'updater_url' => Flight::util()::action('CustomersController@edit', ['id' => $updater->id]),
            'updated' => date(self::TIME_FORMAT, $entity->updated_at).' ('.Flight::util()::dume($entity->updated_at - time(), true).')'
        ]);
    }

    private static function model_user()
    {
        return Flight::util()::model('Customer');
    }

}
