<?php

class RentalProcessController {

    /**
     * Bietet eine Übersichtstabelle von allen abgeschlossenen Verleihprozessen
     */
    static function history()
    {
        $count_returned = self::model()
            ->where('returned_at > ', 0)
            ->count();
        $count_delayed = self::model()
            ->where('end_time < returned_at')
            ->where('AND returned_at > ', 0)
            ->count();

        $statistics = 'Es gibt insgesamt '.Flight::util()::quantize('ein abgeschlossen Verleihprozess', '%s abgeschlossene Verleihprozesse.', $count_returned)
            .' Davon '.Flight::util()::quantize('wurde ein Gerät', 'wurden %s Geräte', $count_delayed).' verspätet zurückgegeben.';

        $process = self::model()
            ->where('returned_at > ', 0);

        $pagination = Flight::util()::pagination_helper();

        $amount_result = self::search($process)->count();
        $results = self::search($process)
            ->order_by('returned_at DESC')
            ->limit($pagination['limit'], $pagination['offset'])
            ->all();

        $all_types = Flight::util()::model('DeviceType')->all();

        Flight::util()::render('rental-process/history', [
            'title' => 'Verleihhistorie',
            'rental_process' => $results,
            'amount_result' => $amount_result,
            'statistics' => $statistics,
            'all_types' => $all_types,
            'query_inventorynumber' =>  Flight::request()->query['query_inventorynumber'] ?? '',
            'query_device_type_id' => Flight::request()->query['query_device_type_id'] ?? '',
            'query_customer' => Flight::request()->query['query_customer'] ?? ''
        ]);
    }

    /**
     * Bietet eine Übersichtstabelle von allen aktiven Verleihprozessen
     */
    static function active()
    {
        $process = self::model();

        if (Flight::session()->is_customer()) {
            $process->where('customer_id = ', Flight::session()->user()->id);
            $page_title = 'Übersicht';
        } else {
            $process->where('returned_at = ', 0);
            $statistics = self::active_statistics();
            $page_title = 'Aktive Verleihe';
        }

        $all_types = Flight::util()::model('DeviceType')->all();

        $pagination = Flight::util()::pagination_helper();

        $total_result_count = self::search($process)->count();
        $result = self::search($process)
            ->order_by('end_time DESC')
            ->limit($pagination['limit'], $pagination['offset'])
            ->all();

        Flight::util()::render('rental-process/active', [
            'title' => $page_title,
            'amount_result' => $total_result_count,
            'rental_process' => $result,
            'statistics' => $statistics ?? '',
            'all_types' => $all_types,
            'query_status' => Flight::request()->query['query_status'] ?? '',
            'query_inventorynumber' =>  Flight::request()->query['query_inventorynumber'] ?? '',
            'query_device_type_id' => Flight::request()->query['query_device_type_id'] ?? '',
            'query_customer' => Flight::request()->query['query_customer'] ?? ''
        ]);
    }

    /**
     * Returns the current amount of all delayed RentalProcess for a an ajax call
     */
    public static function delayed()
    {
        echo self::model()
            ->where('end_time < ', time())
            ->where('AND returned_at = ', 0)
            ->count();
    }

    /**
     * Clear the history of rental process that are older than a year
     */
    public static function reset()
    {
        try {
            $year = 365 * 24 * 60 * 60;
            $all_process = Flight::util()::model('RentalProcess')
                ->where('end_time < ', time() - $year)
                ->where('AND returned_at > ', 0)
                ->all();

            foreach ($all_process as $process) {
                $process->delete();
            }

            Flight::log()::info(sprintf('removed %s rental process (older than a year) from the database', count($all_process)));
            Flight::util()::success(sprintf('Es wurden %s erfolgreich gelöscht.', Flight::util()::quantize('ein Verleih', '%s Verleihe', count($all_process))));
            Flight::util()::back();
        } catch (\Exception $e) {
            Flight::util()::failure($e->getMessage());
            Flight::util()::back();
        }

    }

    private static function active_statistics()
    {
        $count_delayed = self::model()
            ->where('end_time < ', time())
            ->where('AND returned_at = ', 0)
            ->count();
        $count_borrowed = self::model()
            ->where('returned_at = ', 0)
            ->where('AND collected_at > ', 0)
            ->count();
        $count_reserved = self::model()
            ->where('returned_at = ', 0)
            ->where('AND collected_at = ', 0)
            ->where('AND start_time < ', time())
            ->count();
        $count_active = self::model()
            ->where('returned_at = ', 0)
            ->count();

        return 'Es '.Flight::util()::quantize('ist ein Gerät', 'sind %s Geräte', $count_reserved).' reserviert und '
            .Flight::util()::quantize('ein Gerät', '%s Geräte', $count_borrowed).' ausgeliehen (davon '
            .Flight::util()::quantize('ist ein Verleih', 'sind %s Verleihe', $count_delayed).' überzogen). '
            .'Von insgesamt '. Flight::util()::quantize('einem aktiven Verleihprozess', '%s aktiven Verleihprozessen', $count_active).'.';
    }

    private static function search($process)
    {
        if (!empty(Flight::request()->query['query_status'])) {
            $query_status = Flight::request()->query['query_status'];

            if ($query_status == 'planned') {
                $process->where('AND start_time > ', time());
            } else if ($query_status == 'reserved') {
                $process
                    ->where('AND collected_at = ', 0)
                    ->where('AND start_time < ', time());
            } else if ($query_status == 'collected') {
                $process->where('AND collected_at > ', 0);
            } else if ($query_status == 'delayed') {
                $process->where('AND end_time < ', time());
            }
        }

        if (!empty(Flight::request()->query['query_inventorynumber'])) {
            $query_inventorynumber = Flight::request()->query['query_inventorynumber'];

            $device = Flight::util()::model('Device')
                ->where('inventory_number = ', $query_inventorynumber)
                ->first();

            $process->where('AND device_id = ', $device->id ?? 'NULL');
        }

        if (!empty(Flight::request()->query['query_device_type_id'])) {
            $query_device_type_id = Flight::request()->query['query_device_type_id'];

            $devices = Flight::util()::model('Device')
                ->where('device_type_id = ', $query_device_type_id)
                ->all();

            $device_ids = array_map(function($device) {
                return $device->id;
            }, $devices);
            $process->where('AND device_id IN ('.implode($device_ids, ', ').')');
        }

        if (!empty(Flight::request()->query['query_customer'])) {
            $query_customer = Flight::request()->query['query_customer'];

            $customers = Flight::util()::model('Customer')
                ->where('firstname LIKE ', '%'.$query_customer.'%')
                ->where('OR lastname LIKE ', '%'.$query_customer.'%')
                ->where('OR id = ', $query_customer)
                ->all();

            $customer_ids = array_map(function($customer) {
                return $customer->id;
            }, $customers);
            $process->where('AND customer_id IN ('.implode($customer_ids, ', ').')');
        }

        return $process;
    }

    /**
     * Sets the returned_at timestamp to now, which moves the RentalProcess from the status collected to returned.
     * It aborts when it was already returned or when the process was never collected.
     *
     * @param Number Id of RentalProcess
     */
    static function return($id)
    {
        try {
            if (Flight::session()->is_customer()) {
                Flight::util()::failure('Sie sind nicht berechtigt ein Gerät selber zurückzugeben.');
                Flight::util()::back();
            }

            $process = self::model()->find_or_fail($id);

            if (!$process->was_collected()) {
                Flight::util()::failure(sprintf('Gerät "%s" Nr. %s wurde nie abgeholt und kann somit nicht zurückgegeben werden.',
                    $process->device()->type()->title,
                    $process->device()->inventory_number
                ));
                Flight::util()::back();
            }

            if ($process->is_returned()) {
                Flight::util()::failure(sprintf('Gerät "%s" Nr. %s wurde bereits am %s zurückgegeben (%s).',
                    $process->device()->type()->title,
                    $process->device()->inventory_number,
                    $process->get_returned_time(),
                    Flight::util()::dume($process->returned_at - time(), true)
                ));
                Flight::util()::back();
            }

            $process->return_as(Flight::session()->user()->id);

            Flight::util()::success(sprintf('Gerät "%s" Nr. %s erfolgreich zurückgegeben und hatte %s. Die gesamte Ausleihdauer betrug %s.',
                $process->device()->type()->title,
                $process->device()->inventory_number,
                ($process->was_delayed()) ? Flight::util()::dume($process->end_time - time()).' Überzug' : 'keinen Überzug',
                Flight::util()::dume($process->collected_at - time())
            ));
            Flight::util()::back();
        } catch (\Exception $e) {
            Flight::util()::failure($e->getMessage());
            Flight::util()::back();
        }

    }

    /**
     * Sets the collected_at timestamp to now, which moves the RentalProcess from the status planned/reserved to collected.
     * It aborts when it was already collected or the start date is still in the future or if the device is not returned from its last rentalprocess.
     *
     * @param Number Id of RentalProcess
     */
    public static function collect($id)
    {
        try {
            if (Flight::session()->is_customer()) {
                Flight::util()::failure('Sie sind nicht berechtigt ein Gerät selber abzuholen.');
                Flight::util()::back();
            }

            $process = self::model()->find_or_fail($id);

            if ($process->was_collected()) {
                Flight::util()::failure(sprintf('Gerät "%s" Nr. %s wurde bereits am %s abgeholt (%s).',
                    $process->device()->type()->title,
                    $process->device()->inventory_number,
                    $process->get_collected_time(),
                    Flight::util()::dume($process->collected_at - time(), true)
                ));
                Flight::util()::back();
            }

            if ($process->is_planned()) {
                Flight::util()::failure(sprintf('Gerät "%s" Nr. %s kann erst am Startdatum, den %s (Startet %s) abgeholt werden.',
                    $process->device()->type()->title,
                    $process->device()->inventory_number,
                    $process->get_start_time(),
                    Flight::util()::dume($process->start_time - time(), true)
                ));
                Flight::util()::back();
            }

            $process_not_returned_with_same_device = self::model()
                ->where('collected_at > ', 0)
                ->where('AND returned_at = ', 0)
                ->where('AND device_id = ', $process->device_id)
                ->count();

            if ($process_not_returned_with_same_device > 0) {
                Flight::util()::failure(sprintf('Gerät "%s" Nr. %s kann zurzeit nicht ausgeliehen werden, da es noch nicht zurückgegeben wurde.',
                    $process->device()->type()->title,
                    $process->device()->inventory_number
                ));
                Flight::util()::back();
            }

            $process->collect_as(Flight::session()->user()->id);

            Flight::util()::success(sprintf('Gerät "%s" Nr. %s erfolgreich abgeholt. Das Gerät muss bis zum %s (%s) zurückgeben werden.',
                $process->device()->type()->title,
                $process->device()->inventory_number,
                $process->get_end_time(),
                Flight::util()::dume($process->end_time - time(), true)
            ));
            Flight::util()::back();
        } catch (\Exception $e) {
            Flight::util()::failure($e->getMessage());
            Flight::util()::back();
        }
    }

    /**
     * Deletes the RentalProcess from the database.
     * It aborts when the Process was ever collected.
     *
     * @param Number Id of RentalProcess
     */
    static function delete($id)
    {
        try {
            $process = self::model()->find_or_fail($id);

            if (Flight::session()->is_customer() && Flight::session()->user()->id != $process->customer_id) {
                Flight::util()::failure('Sie sind nicht berechtigt Verleihprozesse zu löschen die nicht Ihnen zugeordnet sind.');
                Flight::util()::back();
            }

            if ($process->was_collected()) {
                Flight::util()::failure(sprintf('Der Verleihprozess des Geräts "%s" Nr. %s kann nicht gelöscht werden, da der Prozess bereits im Status "Abgeholt" war.',
                    $process->device()->type()->title,
                    $process->device()->inventory_number
                ));
                Flight::util()::back();
            }

            $process->delete();

            Flight::util()::success(sprintf('Der Verleihprozess des Geräts "%s" Nr. %s wurde erfolgreich gelöscht.',
                $process->device()->type()->title,
                $process->device()->inventory_number
            ));
            Flight::util()::back();
        } catch (\Exception $e) {
            Flight::util()::failure($e->getMessage());
            Flight::util()::back();
        }
    }

    private static function model()
    {
        return Flight::util()::model('RentalProcess');
    }

}
