<?php

class CustomersController {

    const TITLE = 'Kunde';
    const SUCCESS_MESSAGE = '{mask}%s{/mask} (Kundennr. %s) erfolgreich %s';

    /**
     * Bietet eine Übersichtstabelle von allen Kunden mit einer Suche
     */
    static function index()
    {
        $customers = self::model();
        if (!empty(Flight::request()->query['query'])) {
            $query = Flight::request()->query['query'];
            $customers->where('firstname LIKE ', '%'.$query.'%')
                ->where('OR lastname LIKE ', '%'.$query.'%')
                ->where('OR username LIKE ', '%'.$query.'%')
                ->where('OR email LIKE ', '%'.$query.'%')
                ->where('OR id LIKE ', '%'.$query.'%')
                ->where('OR trust_level = ', $query);
        }

        $pagination = Flight::util()::pagination_helper();

        Flight::util()::render('customer/overview', [
            'total_result_count' => $customers->count(),
            'customers' => $customers->limit($pagination['limit'], $pagination['offset'])->all(),
            'query' => $query ?? '',
            'title' => 'Kundenverwaltung'
        ]);
    }

    /**
     * Bietet ein Erstellformular für einen Nutzer
     */
    static function create()
    {
        Flight::util()::render('customer/detail', [
            'customer' => self::model(),
            'title' => 'Neuer '.self::TITLE
        ]);
    }

    /**
     * Speichert einen neuen Nutzer in der Datenbank und leitet anschließend zur Detailseite des neuen Nutzers
     */
    static function store()
    {
        try {
            $customer = self::model()
                ->fill_with_auth(Flight::request()->data)
                ->create_as(Flight::session()->user()->id);

            Flight::util()::success(sprintf(self::SUCCESS_MESSAGE, $customer->name(), $customer->id, 'erstellt'));
            Flight::redirect(Flight::util()::action('@edit', ['id' => $customer->id]));
        } catch (Exception $e) {
            Flight::util()::failure($e->getMessage());
            Flight::util()::back();
        }
    }

    /**
     * Bietet ein Bearbeitungsformular für einen bestehenden Kunden mit gegebener Id
     * @param Any Kundennummer
     */
    static function edit($id = null)
    {
        $id = $id ?? Flight::session()->user()->id;

        try {
            $customer = self::model()->find_or_fail($id);
            Flight::util()::render('customer/detail', [
                'customer' => $customer,
                'title' => $customer->name()
            ]);
        } catch (Exception $e) {
            self::handle_error($e);
        }
    }

    /**
     * Überschreibt den Kunden mit gegebener Id mit neuen Daten und leitet anschließend zur Detailseite
     * @param Any Kundennummer
     */
    static function update($id = null)
    {
        $id = $id ?? Flight::session()->user()->id;

        try {
            $customer = self::model()
                ->find_or_fail($id);

            if ($customer->is_admin() && !Flight::session()->is_admin()) {
                Flight::util()::failure('Sie sind nicht berechtigt diesen Datensatz zu ändern');
                Flight::log()::info('only admins can change other admins authorization middleware failed');
                Flight::util()::back();
            }

            $customer
                ->fill_with_auth(Flight::request()->data)
                ->update_as(Flight::session()->user()->id);

            Flight::util()::success(sprintf(self::SUCCESS_MESSAGE, $customer->name(), $customer->id, 'gespeichert'));
            Flight::util()::back();
        } catch (Exception $e) {
            self::handle_error($e);
        }
    }

    /**
     * Löscht den Nutzer mit angegebener Nummer und leitet dann zur Übersicht weiter
     * @param Any Kundennummer
     */
    static function delete($id)
    {
        try {
            $customer = self::model()
                ->find_or_fail($id)
                ->delete();

            Flight::util()::success(sprintf(self::SUCCESS_MESSAGE, $customer->name(), $customer->id, 'gelöscht'));
            Flight::redirect('@index');
        } catch (Exception $e) {
            self::handle_error($e);
        }
    }

    /**
     * Im Falle eines 404 Errors leitet der Error Handler in jedem Fall zur Übersicht
     * ansonsten wird zurückgeleitet mit der Message aus der Exception
     * @param \Exception
     */
    private static function handle_error($error)
    {
        if ($error->getCode() == 404) {
            Flight::util()::failure(self::TITLE.' nicht gefunden');
            Flight::redirect('@index');
        } else {
            Flight::util()::failure($error->getMessage());
            Flight::util()::back();
        }
    }

    /**
     * Erstellt eine Customer Instanz
     * @return Customer
     */
    private static function model()
    {
        return Flight::util()::model('Customer');
    }
}
