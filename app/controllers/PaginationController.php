<?php

class PaginationController {


    public static function show($action, $amount_result, $amount_page)
    {
        $query = Flight::request()->query->getData();
        $limit = Flight::get('app.results.default_limit');

        $page = (array_key_exists('page', $query)) ? (int) $query['page'] : 0;

        $max_page = (int) ($amount_result / $limit);
        if ($page + 1 == $max_page && $amount_result % $limit == 0) {
            $max_page--;
        }

        $next = ($page < $max_page) ? self::link($query, $page + 1, $action) : '';
        $previous = ($page > 0) ? self::link($query, $page - 1, $action) : '';

        Flight::render('pagination', [
            'link_next' => $next,
            'link_previous' => $previous,
            'current_page_number' => $page + 1,
            'max_page_number' => $max_page + 1,
            'amount_page' => $amount_page
        ]);
    }

    private static function link($query, $page, $action)
    {
        return $action.'?'.http_build_query(array_merge($query, ['page' => $page]));
    }

}
