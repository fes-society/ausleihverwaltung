<?php

class NewRentalProcess {

    public $customer_id;
    public $device_ids;
    public $start_time;
    public $end_time;
    public $comment;

    public function __construct($data)
    {
        $this->customer_id = $data['customer_id'] ?? null;
        $this->device_ids = $data['device_ids'] ?? [];
        $this->start_time = $data['start_time'] ?? '';
        $this->end_time = $data['end_time'] ?? '';
        $this->comment = $data['comment'] ?? '';
    }

    public function set_start_time($date)
    {
        $this->start_time = (new DateTime($date))->format(DateTime::ATOM);
    }

    public function set_end_time($date)
    {
        $this->end_time = (new DateTime($date))->format(DateTime::ATOM);
    }

    public function get_start_time()
    {
        return (new DateTime($this->start_time))->format('d.m.Y H:i');
    }

    public function get_end_time()
    {
        return (new DateTime($this->end_time))->format('d.m.Y H:i');
    }

    public function total_time()
    {
        return (new DateTime($this->end_time))->getTimestamp() - (new DateTime($this->start_time))->getTimestamp();
    }

}
