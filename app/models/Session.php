<?php

class Session {

    protected $user;

    function is_admin()
    {
        return $this->user->is_admin();
    }

    function is_agent()
    {
        return $this->user->is_agent();
    }

    function is_customer()
    {
        return $this->user->is_customer();
    }

    function is_logged_in()
    {
        return !empty($this->user);
    }

    function user()
    {
        return $this->user;
    }

    function set($user)
    {
        $this->user = $user;
    }

}
