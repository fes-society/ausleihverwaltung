<?php

class RentalProcess extends Kiwi {

    public $id;
    public $device_id;
    public $customer_id;
    public $start_time = 0;
    public $end_time = 0;
    public $returned_at = 0;
    public $returned_by = 0;
    public $collected_at = 0;
    public $collected_by = 0;
    public $created_at = 0;
    public $created_by = 0;
    public $comment = '';

    protected $device;
    protected $customer;

    /**
     * Kiwi Configuration
     */
    protected static $primary_key = 'id';
    protected static $table = 'RentalProcess';

    /**
     * Properties that are not mass assignable
     */
    protected $guarded = [
        'returned_by',
        'collected_by', 'collected_at',
        'created_by', 'created_at'
    ];

    public function device() {
        if (empty($this->device)) {
            $this->device = (new Device($this->database))->find($this->device_id);
        }
        return $this->device;
    }

    public function customer() {
        if (empty($this->customer)) {
            $this->customer = (new Customer($this->database))->find($this->customer_id);
        }
        return $this->customer;
    }

    public function collect_as($key)
    {
        $this->collected_at = time();
        $this->collected_by = $key;
        return $this->update();
    }

    public function return_as($key)
    {
        $this->returned_at = time();
        $this->returned_by = $key;
        return $this->update();
    }

    public function create_as($key)
    {
        $this->created_at = time();
        $this->created_by = $key;
        return $this->create();
    }

    public function set_start_time($date)
    {
        $this->start_time = (new DateTime($date))->format(DateTime::ATOM);
    }

    public function set_end_time($date)
    {
        $this->end_time = (new DateTime($date))->format(DateTime::ATOM);
    }

    public function get_start_time()
    {
        return (new DateTime())->setTimestamp($this->start_time)->format('d.m.Y H:i');
    }

    public function get_end_time()
    {
        return (new DateTime())->setTimestamp($this->end_time)->format('d.m.Y H:i');
    }

    public function get_collected_time()
    {
        return (new DateTime())->setTimestamp($this->collected_at)->format('d.m.Y H:i');
    }

    public function get_returned_time()
    {
        return (new DateTime())->setTimestamp($this->returned_at)->format('d.m.Y H:i');
    }

    public function total_time()
    {
        return $this->end_time - $this->start_time;
    }

    public function actual_total_time()
    {
        return $this->returned_at - $this->collected_at;
    }

    /**
     * Whether the RentalProcess is currently delayed.
     *
     * @return Boolean
     */
    public function is_delayed()
    {
        return $this->end_time < time();
    }

    /**
     * Whether the RentalProcess was ever delayed.
     *
     * @return Boolean
     */
    public function was_delayed()
    {
        return $this->end_time < $this->returned_at;
    }

    /**
     * Whether the RentalProcess is currently collected and not returned.
     *
     * @return Boolean
     */
    public function is_collected()
    {
        return $this->collected_at > 0 && $this->returned_at == 0;
    }

    /**
     * Whether the RentalProcess was ever collected.
     *
     * @return Boolean
     */
    public function was_collected()
    {
        return $this->collected_at > 0;
    }

    public function is_reserved()
    {
        return !$this->is_planned() && $this->collected_at == 0 && $this->returned_at == 0;
    }

    public function is_returned()
    {
        return $this->returned_at > 0;
    }

    public function is_planned()
    {
        return $this->start_time > time();
    }

    public function is_overlapping_with($start_new, $end_new)
    {
        $process_end = ($this->is_delayed()) ? time() : $this->end_time;
        return $this->start_time < $end_new && $start_new < $process_end;
    }

}
