<?php

class Device extends KiwiMeta {

    public $id;
    public $inventory_number;
    public $device_type_id;
    public $condition;
    public $manufacturer;
    public $model;
    public $buy_date;
    public $guarantee_duration;
    public $storage_location;
    public $comment;
    public $equipment;
    public $trust_level = 0;

    protected $device_type;

    /**
     * Kiwi Configuration
     */
    protected static $primary_key = 'id';
    protected static $table = 'Devices';

    /**
     * Properties die nicht mass assignable sind
     */
    protected $guarded = [];

    /**
     * Allows search for multiple devices by id
     */
    public function where_bulk_id($ids)
    {
        $this->where('id == ', -1);
        foreach ($ids as $id) {
            $this->where('OR id = ', $id);
        }
        return $this;
    }

    /**
     * Returns a DeviceType for this device
     * @return DeviceType
     */
    public function type()
    {
        if (empty($this->device_type)) {
            $this->device_type = (new DeviceType($this->database))
                ->find($this->device_type_id);
        }
        return $this->device_type;
    }

    public function get_buy_date()
    {
        if (empty($this->buy_date)) {
            return '';
        }
        return (new DateTime($this->buy_date))->format('d.m.Y');
    }

    /**
     * Validation hook of kiwi that is executed when updating, creating or deleting a customer
     * @param Device
     * @param String The origin of the operation (update, delete, create)
     */
    protected static function validate (Device $device, $operation)
    {
        if ($operation == self::OPERATION_CREATE || $operation == self::OPERATION_UPDATE) {
            if (empty($device->inventory_number)) {
                $errors[] = 'Es wurde keine Inventarnummer angegeben';
            } else {
                $query = (new $device($device->database))
                    ->where('inventory_number = ', $device->inventory_number);
                if ($device->id) {
                    $query->where('AND id <> ', $device->id);
                }

                if ($query->count() > 0) {
                    $errors[] = 'Die Inventarnummer '.$device->inventory_number.' wurde bereits einem anderen Gerät vergeben';
                }
            }
            if (empty($device->device_type_id)) {
                $errors[] = 'Es wurde kein Gerätetyp angegeben';
            }
        }
        return $errors ?? [];
    }

    /**
     * Returns a boolean which says whether this device is currently available for reservations
     * @return Boolean
     */
    function is_available()
    {
        return (new RentalProcess($this->database))
            ->where('device_id = ', $this->id)
            ->where('AND returned_at = ', 0)
            ->count() > 0;
    }

    /**
     * Returns a boolean which says whether this device is currently borrowed and therefore not available
     * @return Boolean
     */
    function borrowed()
    {
        return (new RentalProcess($this->database))
            ->where('device_id = ', $this->id)
            ->where('AND collected_at > ', 0)
            ->where('AND returned_at = ', 0)
            ->count() > 0;
    }

    /**
     * Returns a boolean which says whether this device has a planned or reserved rental process
     * @return Boolean
     */
    function reserved_or_planned()
    {
        return (new RentalProcess($this->database))
            ->where('device_id = ', $this->id)
            ->where('AND collected_at = ', 0)
            ->where('AND returned_at = ', 0)
            ->count() > 0;
    }

    /**
     * Returns the amount of all rental process that use/used this device
     * @return Number
     */
    function borrowed_total_amount()
    {
        return (new RentalProcess($this->database))
            ->where('device_id = ', $this->id)
            ->count();
    }

    /**
     * Returns the amount of returned rental process which were delayed
     * @return Number
     */
    function delayed_total_amount()
    {
        return (new RentalProcess($this->database))
            ->where('device_id = ', $this->id)
            ->where('AND returned_at > ', 0)
            ->where('AND end_time < returned_at')
            ->count();
    }
}
