<?php

class DeviceType extends Kiwi {

    public $id;
    public $title;
    public $description;

    /**
     * Kiwi Configuration
     */
    protected static $primary_key = 'id';
    protected static $table = 'DeviceTypes';

    /**
     * Properties that are not mass assignable
     */
    protected $guarded = [];

    /**
     * Validation hook of kiwi that is executed when updating, creating or deleting a customer
     * @param DeviceType
     * @param String The origin of the operation (update, delete, create)
     */
    protected static function validate(DeviceType $device_type, $operation)
    {
        if ($operation == self::OPERATION_CREATE || $operation == self::OPERATION_UPDATE) {
            if (empty($device_type->title)) {
                $errors[] = 'Titel ist ein Pflichtfeld';
            }
        }
        return $errors ?? [];
    }

}
