<?php

class Customer extends KiwiMeta {

    /**
     * Primary Key
     */
    public $id;

    /**
     * Person
     */
    public $firstname;
    public $lastname;
    public $identity_card_number;
    public $date_of_birth = 0;

    /**
     * Address
     */
    public $street;
    public $street_number;
    public $postcode;
    public $place;

    /**
     * Contact
     */
    public $email;
    public $phone_number;
    public $phone_number_legal_guardian;

    /**
     * Admin settings
     */
    public $accepted_external_use_conditions = 0;
    public $trust_level = 0;

    /**
     * Authentication and Authorization
     */
    public $username;
    public $password;
    public $role = 'none';

    /**
     * Kiwi Configuration
     */
    protected static $primary_key = 'id';
    protected static $table = 'Customers';

    /**
     * Properties that are not mass assignable
     */
    protected $guarded = [
        'role',
        'username',
        'password',
        'trust_level',
        'accepted_external_use_conditions'
    ];

    /**
     * Builds a fullname from the first- and lastname properties
     * @return String
     */
    function name()
    {
        if (!$this->firstname && !$this->lastname) return $this;
        if ($this->firstname) $result[] = $this->firstname;
        if ($this->lastname) $result[] = $this->lastname;
        return implode($result, ' ');
    }

    /**
     * Returns the amount of all active rental process for this customer
     * @return Number
     */
    function active_issues()
    {
        return (new RentalProcess($this->database))
            ->where('customer_id = ', $this->id)
            ->where('AND returned_at = ', 0)
            ->count();
    }

    /**
     * Returns the amount of all delayed process that are not yet returned by this customer
     * @return Number
     */
    function delayed_process()
    {
        return (new RentalProcess($this->database))
            ->where('customer_id = ', $this->id)
            ->where('AND returned_at = ', 0)
            ->where('AND end_time < ', time())
            ->count();
    }

    /**
     * Returns the amount of all returned process by this customer
     * @return Number
     */
    function returned_process()
    {
        return (new RentalProcess($this->database))
            ->where('customer_id = ', $this->id)
            ->where('AND returned_at > ', 0)
            ->count();
    }

    /**
     * Returns the amount of all returned but delayed process by this customer
     * @return Number
     */
    function returned_delayed_process()
    {
        return (new RentalProcess($this->database))
            ->where('customer_id = ', $this->id)
            ->where('AND returned_at > ', 0)
            ->where('AND end_time < returned_at')
            ->count();
    }

    /**
     * Checks whether the given password equals the hash from this customer
     * @param String
     * @return Boolean
     */
    public function verify_password($password)
    {
        return password_verify(base64_encode(hash('sha256', $password, true)), $this->password);
    }

    /**
     * Sets and encrypts a new password for this customer
     * @param String
     */
    public function set_new_password($password)
    {
        $this->password = password_hash(base64_encode(hash('sha256', $password, true)), PASSWORD_DEFAULT);
    }

    /**
     * Validation hook of kiwi that is executed when updating, creating or deleting a customer
     * @param Customer
     * @param String The origin of the operation (update, delete, create)
     */
    protected static function validate (Customer $customer, $operation)
    {
        if ($operation == self::OPERATION_CREATE || $operation == self::OPERATION_UPDATE) {
            if (empty($customer->firstname) && empty($customer->lastname)) {
                $errors[] = 'Es wurde kein Vor- oder Nachnamen angegeben';
            }

            // At least one way of contact has to be present
            if (empty($customer->email) && empty($customer->phone_number)) {
                $errors[] = 'Es wurde keine E-Mail oder Telefonnummer angegeben';
            }

            // Unique username
            if (!empty($customer->username)) {
                $query = (new $customer($customer->database))
                    ->where('username = ', $customer->username);
                if ($customer->id) {
                    $query->where('AND id <> ', $customer->id);
                }

                if ($query->count() > 0) {
                    $errors[] = 'Der Benutzername '.$customer->username.' wurde bereits einem anderen Kunden vergeben';
                }
            }
        }
        return $errors ?? [];
    }

    /**
     * Fills the customer with new data, but handles username, password and role differently.
     * @param Array Data to be filled into the customer
     * @return Object Self reference
     */
    public function fill_with_auth($data)
    {
        $this->username = trim($data->username);
        unset($data->username);

        $this->fill_password($data->password);
        unset($data->password);

        $this->fill_role($data->role);
        unset($data->role);

        if (Flight::session()->is_admin()) {
            $this->trust_level = $data->trust_level;
            $this->accepted_external_use_conditions = $data->accepted_external_use_conditions;
        }
        unset($data->trust_level);
        unset($data->accepted_external_use_conditions);

        return $this->fill($data);
    }

    /**
     * Possible roles that a customer can have
     */
    public function get_allowed_roles()
    {
        return [
            'none' => '-',
            'customer' => 'Kunde',
            'agent' => 'Bearbeiter',
            'admin' => 'Admin'
        ];
    }

    public function is_admin()
    {
        return $this->role === 'admin';
    }

    public function is_agent()
    {
        return $this->role === 'agent';
    }

    public function is_customer()
    {
        return $this->role === 'customer';
    }


    /**
     * Validates the new role
     * @param String
     */
    private function fill_role($new_role)
    {
        if (empty($new_role)) {
            return;
        }

        if (!in_array($new_role, array_keys($this->get_allowed_roles()))) {
            $new_role = $this->get_allowed_roles()['none'];
        }

        if ($this->role == $new_role) {
            return;
        }

        if (Flight::session()->user()->id == $this->id) {
            Flight::log()::info('can not change own role condition failed');
            throw new \Exception('Sie können Ihre eigene Rolle nicht ändern');
        }

        if (!Flight::session()->is_admin()) {
            Flight::log()::info('only admins can change roles condition failed');
            throw new \Exception('Sie haben keine Berechtigungen um Rollen zu ändern');
        }

        Flight::log()::info(sprintf('changed role of %s from %s to %s', $this, $this->role, $new_role));
        $this->role = $new_role;
    }

    /**
     * Validates the new password
     * @param String
     */
    private function fill_password($new_password)
    {
        if (empty($new_password)) {
            return;
        }
        Flight::log()::info(sprintf('changed password of %s', $this));
        $this->set_new_password($new_password);
    }

}
