# Customer Authorizations

| role / rights | update self* | read | create* | update* | delete | create/update admin settings** | create/update/delete admin |
|---------------|--------------|------|---------|---------|--------|--------------------------------|----------------------------|
| none |  |  |  |  |  |  |  |
| customer | x |  |  |  |  |  |  |
| agent | x | x | x | x |  |  |  |
| admin | x | x | x | x | x | x | x |

* \* except admin settings
* \*\* admin settings fields: role, trust level, accepted_external_use_conditions
